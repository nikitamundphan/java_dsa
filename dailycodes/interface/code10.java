interface Demo1{
	static void fun(){
		System.out.println("In demo1-fun");
	}
}
interface Demo2{
	static void fun(){
		System.out.println("In Demo2-fun");
		Demo1.fun();
	}
}
class Demo3 implements Demo1,Demo2{
	void fun(){
		System.out.println("In Demo-fun");
		Demo2.fun();
	}
}
class Client{
	public static void main(String[] args){
		Demo3 obj = new Demo3();
		obj.fun();
	}
}

