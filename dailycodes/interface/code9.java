interface Demo{
	static void fun(){
		System.out.println("In fun");
	}
}
class ChildDemo implements Demo{
	void fun(){
		System.out.println("In Demochild-fun");
		Demo.fun();
	}
}
class Client{
	public static void main(String[] args){
		ChildDemo obj=new ChildDemo();
		obj.fun();
	}
}

		
