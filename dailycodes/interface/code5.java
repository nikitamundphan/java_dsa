interface Demo{
	void fun();
	default void gun(){
		System.out.println("In gun-demo");
	}
}
class ChildDemo implements Demo{
	public void fun(){
		System.out.println("In fun-childDemo");
	}
}
class Client{
	public static void main(String[] args){
		Demo obj1=new ChildDemo();
		obj1.fun();
		obj1.gun();
	}

}

