interface Demo{
	void fun();
}
interface Demo2{
	void fun();
}
class DemoChild implements Demo,Demo2{
	public void fun(){
		System.out.println("In fun child");
	}
}
class Client{
	public static void main(String[] args){
		Demo obj1=new DemoChild();
		obj1.fun();

		Demo2 obj2=new DemoChild();
		obj2.fun();
	}
}
