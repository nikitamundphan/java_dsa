// b. prefix sum
class Prefixsum{
	public static void main(String[] args){
		int arr[]=new int[]{2,4,1,3};
		int n=arr.length;

		int pref[]=new int[n];
		pref[0]=arr[0];
		for(int i=1;i<n;i++){
			pref[i]=pref[i-1]+arr[i];
		}
		for(int i=0;i<n;i++){
			for(int j=i;j<n;j++){
				int sum=0;
				if(i==0){
					sum=pref[j];
				}
				else{
					sum=pref[j]-pref[i-1];
				}
				System.out.println(sum);
			}
		}
	}

}

