//given an array of size n .count the no of elements having atleast one element graeter than itself
class Demo{
	static void count(int arr[],int n){
		int count=0;
		int max=Integer.MIN_VALUE;

		for(int i=0;i<n;i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		for(int i=0;i<n;i++){
			if(arr[i] !=max){
				count++;
			}
		}
		System.out.println(count);
	}
	public static void main(String[] args){
		int arr[]=new int[]{2,5,1,4,8,0,8,1,3,8};
		int n=arr.length;
		count(arr,n);
	}
}

