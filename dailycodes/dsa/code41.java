// given square matrix n*m
// rotate the matrix by 90 degree in clockwise direction
class Demo{
	public static void main(String[] args){
		int arr[][]=new int [][]{{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		int n= arr.length;
		for(int i=0;i<n;i++){
			for(int j=i+1;j<n;j++){
				int temp=arr[i][j];
				arr[i][j]=arr[j][i];
				arr[j][i]=temp;
			}
		}
		for(int i=0;i<n;i++){
			int start=0;
			int end=n-1;
			while(start<end){
				int temp=arr[i][start];
				arr[i][start]=arr[end][i];
				arr[end][i]=temp;
				start++;
				end--;
			}
		}
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				System.out.print(arr[i][j]+ " ");
			}
			System.out.println();
		}
	}
}


