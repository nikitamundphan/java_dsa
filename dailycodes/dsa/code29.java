// prefix sum approach
class Demo{
	public static void main(String[] args){
		int arr[]=new int[]{-2,1,-3,4,-1,2,1,-5,4};
		int pref[]=new int[arr.length];
		pref[0]=arr[0];
		for(int i=1;i<arr.length;i++){
			pref[i]=pref[i-1]+arr[i];
		}
		int sum=0;
		int max=Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){
				if(i==0){
					sum=pref[j];
				}
				else{
					sum=pref[j]+pref[i-1];
					if(sum>max){
						max=sum;
					}
				}
			}
		}
		System.out.println(max);
	}
}

