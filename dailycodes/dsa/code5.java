// reverse the array of time complexity o(n),space complexity o(n);
class Demo{
	static void Reverse(int arr[],int n){
		for(int i=n;i>=0;i++){
			System.out.println(arr[i]);
		}
	}
	public static void main(String[] args){
		int arr[]=new int[]{8,4,1,3,9,2,6,7};
		int n=arr.length;
		Reverse(arr,n);
	}
}

