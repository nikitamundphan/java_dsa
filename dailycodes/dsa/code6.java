//reverse the array without using extra space complexity
class Demo{
	public static void main(String[] args){
		int arr[]=new int[]{8,4,1,3,9,2,6,7};
		int n=arr.length;
		int i=0;
		int j=n-1;
		while(i!=j){
			int temp=arr[i];
			arr[i]=arr[j];
			temp=arr[j];
			 i++;
			 j--;
		       System.out.println(arr[i]);
		}
	}
}

