// Given an array of size n return the count of pairs (i,j) with arr[i]+arr[j]=k
// n=10
// k=10
class Demo{
	static void Pair(int arr[],int n,int k){
		int count=0;

		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				if(arr[i]+arr[j]==k && i!=j){
					count++;
				}
			}
		}
		System.out.println(count);
	}
	public static void main(String[] args){
		int arr[]=new int[]{3,5,2,1,-3,7,8,15,6,13};
		int n=arr.length;
		int k=10;
		Pair(arr,n,k);
	}
}


