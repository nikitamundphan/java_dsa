//total sum of all subarray 
class TotalDemo{
	public static void main(String [] args){
		int arr[]=new int[]{1,2,3};
		int n=arr.length;
		int total=0;
		for(int i=0;i<n;i++){
			int sum=0;
			for(int j=i;j<n;j++){
				sum=sum+arr[j];
				total=total+sum;
			}
		}
		System.out.println(total);

	}
}
