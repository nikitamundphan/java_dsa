//given an array of integer of size n build an array leftmax of size M leftmax of i contains the maximum for the index 0 to i;
class Demo{
	public static void main(String[] args){
		int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int n=arr.length;
		int leftmax[]=new int[n];
		int max=Integer.MIN_VALUE;
		for(int i=0;i<n;i++){
			for(int j=0;j<=i;j++){
				
				if(max<arr[j]){
					max=arr[j];
					leftmax[i]=max;
				}
			}
		}
		for(int i=0;i<n;i++){
			System.out.println(leftmax[i]);
		}
	}
}


