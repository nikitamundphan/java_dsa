//given a square matrix of m*n
//convert the matrix to its transpose
class Demo{
	public static void main(String[] args){
		int arr[][]=new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		int n =arr.length;
		for(int i=0;i<n;i++){
			for(int j=i+1;j<n;j++){
				int temp=arr[i][j];
				arr[i][j]=arr[j][i];
				arr[j][i]=temp;
			}
		}
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){

				System.out.print(arr[i][j]+ " ");
		}
		System.out.println();
		}
	}
}

		

