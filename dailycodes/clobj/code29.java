class Parent{
	Parent(){
		System.out.println(this);
		System.out.println("In parent constructor");
	}
	void parentproperty(){
		System.out.println("Gold");
	}
}
class Child extends Parent{
	Child(){
		System.out.println(this);
		System.out.println("In child constructor");
	}

}
class Client{
	public static void main(String[] args){
		Parent obj1=new Child();
		System.out.println(obj1);
		Parent obj2= new Parent();
		System.out.println(obj2);
		obj1.parentproperty();
	}
}
