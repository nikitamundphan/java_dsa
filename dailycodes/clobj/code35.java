class Parent{
	int x=10;
	static int y=20;
	Parent(){
		System.out.println("parent");
	}
}
class Child extends Parent{
	int x=10;
	static int y=20;
	Child(){
		System.out.println("child");
	}
	void access(){
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
		obj.access();
	}
}
