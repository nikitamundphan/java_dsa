class Parent{
	StringBuffer fun(){
		System.out.println("In parent fun");
		return new StringBuffer("Nikita");
	}
}
class Child extends Parent{
	String fun(){
		System.out.println("In child fun");
		return "Nikita";
	}
}
class Client{
	public static void main(String[] args){
		Parent p=new Child();
		p.fun();
	}
}

