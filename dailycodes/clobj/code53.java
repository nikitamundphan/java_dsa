class Parent{
	final void fun(){
		System.out.println("parent fun");
	}
}
class Child extends Parent{
	void fun(){
		System.out.println("child fun");
	}
}

