class Demo{
	void fun(int x){
		System.out.println(x);
	}
	void fun(float y){
		System.out.println(y);
	}
	void fun(Demo obj){
		System.out.println("In demo para");
		System.out.println(obj);
	}
	public static void main(String[] args){
		Demo obj1=new Demo();
		obj1.fun(10);
		obj1.fun(10.5f);
		Demo obj2= new Demo();
		obj2.fun(obj1);
	}
}


