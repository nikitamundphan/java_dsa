class Parent{
	Parent(){
		System.out.println("In constructor");
	}
	void fun(){
		System.out.println("In parent fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("child constructor");
	}
	void fun(){
		System.out.println("In child fun");
	}
}
class Client{
	public static void main(String[] args){
		Parent obj1=new Child();
		obj1.fun();
	}
}

