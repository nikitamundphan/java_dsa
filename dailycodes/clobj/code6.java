class Demo{
	Demo(){
		System.out.println("In constructor");
	}
	void fun(){
		Demo obj=new Demo();
		System.out.println(obj);
	}
	public static void main(String[] args){
		Demo obj1=new Demo();
		System.out.println(obj1);
		Demo obj2=new Demo();
		System.out.println(obj2);
		obj1.fun();
	}
}
