class Demo{
	void fun(String str){
		System.out.println("string");
	}
	void fun(StringBuffer str1){
		System.out.println("string buffer");
	}
}
class Client{
	public static void main(String[] args){
		Demo obj=new Demo();
		obj.fun("core2web");
		obj.fun(new StringBuffer("core2web"));
		obj.fun(null);
	}
}
