class Defence{
	Defence(){
		System.out.println(this);
	   	System.out.println("In defence");
	}

	void Security(){
		System.out.println("Indian Defence Force");
	}
}
class Army extends Defence{
	Army(){
		System.out.println(this);
		System.out.println("In army");
	}

}
class Client{
	public static void main(String[] args){
		Defence obj1=new Defence();
		obj1.Security();
		System.out.println(obj1);
		Army obj2 = new Army();
		obj2.Security();
		System.out.println(obj2);
	}
}



