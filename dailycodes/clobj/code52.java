class Parent{
	private void fun(){
		System.out.println("parent fun");
	}
}
class Child extends Parent{
	void fun(){
		System.out.println("child fun");
	}
}
class Client{
	public static void main(String[] args){
		Parent obj=new Child();
		obj.fun();
	}
}

