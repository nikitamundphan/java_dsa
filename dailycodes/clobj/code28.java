class Parent{
	Parent(){
		System.out.println(this);
		System.out.println("In parent constructor");
	}
	void parentproperty(){
		System.out.println("flat","car","gold");
	}
}
class Child extends Parent {
	Child(){
		System.out.println(this);
		System.out.println("In child constructor");
	}
}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
		System.out.println(obj);
		obj.parentproperty();
	}
}

