class Parent{
	Parent(){
		System.out.println("In parent constructor");
	}
	void property(){
		System.out.println("Home,car,gold");
	}
	void marry(){
		System.out.println("Deepika padukone");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In child constructor");
	}
	void marry(){
		System.out.println("Alia bhatt");
	}
}
class Client{
	Child obj1=new Child();
	obj1.property();
	obj1.marry();
}


