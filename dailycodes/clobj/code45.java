class Match{
	void matchType(){
		System.out.println("T20/oneDay/Test");
	}
}
class Iplmatch extends Match{
	void matchType(){
		System.out.println("T20");
	}
}
class Testmatch extends Match{
	void matchType(){
		System.out.println("Test");
	}
}
class Client{
	public static void main(String[] args){
		Match type1=new Iplmatch();
		type1.matchType();

		Match type2=new Testmatch();
		type2.matchType();
	}
}

