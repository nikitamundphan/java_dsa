import java.util.*;
class Nvidia implements Comparable{
	String ProductName=null;
	float rating=0;
	Nvidia(String ProductName,float rating){
		this.ProductName=ProductName;
		this.rating=rating;
	}
	public int compareTo(Object obj){
		return (ProductName.compareTo(((Nvidia)obj).ProductName));
	}
	public String toString(){
		return ProductName;
	}
}
class SortListDemo{
	public static void main(String[] args){
		TreeSet ts = new TreeSet();
		ts.add(new Nvidia("GPU",5.9f));
		ts.add(new Nvidia("GTX-1060",4.6f));
		ts.add(new Nvidia("RTX-4090",5.0f));
		System.out.println(ts);
	}
}

