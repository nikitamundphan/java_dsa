import java.util.*;
class Movies{
	String movname=null;
	double totcoll=0.0;
	float imdb=0.0f;
	Movies(String movname,double totcoll,float imdb){
		this.movname=movname;
		this.totcoll=totcoll;
		this.imdb= imdb;
	}
	public String toString(){
		return "{" +movname+ ":" +totcoll+ ":"+imdb+"}";

	}
}
class SortByName implements Comparator{
	public int compare(Movies obj1,Movies obj2){
		return (((Movies)obj1).movname.compareTo((Movies)obj2).movname);
	}
}
class SortBytotcoll implements Comparator{
	public int compare(Movies obj1, Movies obj2){
		return (int)((((Movies)obj1).totcoll) - (((Movies)obj2).totcoll));
	}
}
class SortByIMDB implements Comparator{
	public int compare(Movies obj1, Movies obj2){
		return (int)((((Movies)obj1).imdb) -(((Movies)obj2).imdb));
	}
}
class ListDemo{
	public static void main(String[] args){
		ArrayList al = new ArrayList();
		al.add(new Movies("Gadar2",150.00,4.5f));
		al.add(new Movies("OMG2",100.00,5.0f));
		al.add(new Movies("Boys2",200.00,4.5f));
		System.out.println(al);
		Collections.sort(al,new SortByName());
		System.out.println(al);

		Collections.sort(al,new SortBytotcoll());
		System.out.println(al);

		Collections.sort(al,new SortByIMDB());
		System.out.println(al);

	}
}


