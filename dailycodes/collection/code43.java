import java.util.*;
class NavigableDemo{
	public static void main(String[] args){
		NavigableMap nm =new TreeMap();
		nm.put(10,"Kanha");
		nm.put(20,"Ashish");
		nm.put(30,"Rahul");
		System.out.println(nm);

		System.out.println(nm.lowerEntry(10));
		System.out.println(nm.floorEntry(20));
		System.out.println(nm.ceilingEntry(13));
		System.out.println(nm.higherEntry(25));
		System.out.println(nm.pollFirstEntry());
		System.out.println(nm.pollLastEntry());

		NavigableMap data = nm.descendingMap();
		System.out.println(data);




		
	}
}
