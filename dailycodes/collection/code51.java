import java.util.concurrent.*;
class BlockingQueueDemo{
	public static void main(String[] args)throws InterruptedException{
		LinkedBlockingQueue bq =new LinkedBlockingQueue(3);
		bq.offer(10);
		bq.offer(20);
		bq.offer(30);
		System.out.println(bq);
		bq.put(40);
		System.out.println(bq);
	}
}
