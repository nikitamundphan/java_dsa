import java.util.*;
class NavigableDemo{
	public static void main(String[] args){
		NavigableSet ss = new TreeSet();
		ss.add("kanha");
		ss.add("rahul");
		ss.add("ashish");
		ss.add("badhe");
		System.out.println(ss.lower("ashish"));  
		System.out.println(ss.floor("rahul"));  
		System.out.println(ss.ceiling("badhe"));
	        System.out.println(ss.higher("kanha"));
		System.out.println(ss.pollFirst());
		System.out.println(ss.pollLast());
	}
}

