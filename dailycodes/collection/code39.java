import java.util.*;
class TreeMapDemo{
	public static void main(String[] args){
		SortedMap tm = new TreeMap();
		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("Aus","Australia");
		tm.put("Ban","Banagaladesh");
		System.out.println(tm);
		Set<Map.Entry>data = tm.entrySet();
		Iterator<Map.Entry> itr = data.iterator();
		while(itr.hasNext()){
			Map.Entry abc = itr.next();
			System.out.println(abc.getKey() + ":" +abc.getValue());
		}

	}
}

