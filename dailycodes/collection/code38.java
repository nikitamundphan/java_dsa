import java.util.*;
class TreeMapDemo{
	public static void main(String[] args){
		SortedMap tm = new TreeMap();
		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("Aus","Australia");
		tm.put("Ban","Banagaladesh");
		System.out.println(tm);

		System.out.println(tm.subMap("Aus","Pak"));
		System.out.println(tm.headMap("Pak"));
		System.out.println(tm.tailMap("Pak"));
		System.out.println(tm.firstKey());
		System.out.println(tm.lastKey());
		System.out.println(tm.keySet());
		System.out.println(tm.values());
		System.out.println(tm.entrySet());
	}
}

