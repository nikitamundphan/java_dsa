import java.util.*;
class Movie{
	String movName=null;
	float imdb= 0.0f;
	double totcoll=0.0;
	Movie(String movName,float imdb,double totcoll){
		this.movName=movName;
		this.imdb= imdb;
		this.totcoll= totcoll;
	}
	public String toString(){
		return "{" +movName+ ":"+imdb+ ":"+totcoll + "}";
	}
}
class SortByName implements Comparator<Movie>{
	public int comapre(Movies obj1,Movies obj2){
		return obj1.movName.compareTo(obj2.movName);
	}
}
class SortBytotcoll implements Comparator<Movie>{
	public int compare(Movies obj1, Movies obj2){
		return (int)(obj1.totcoll - obj2.totcoll);
	}
}
class SortByIMDB implements Comparator<Movie>{
	public int compare(Movies obj1,Movies obj2){
		return (int)(obj1.imdb - obj2.imdb);
	}
}

class ListDemo{
	public static void main(String[] args){
		ArrayList<Movie>al= new ArrayList<Movie>();
		al.add(new Movie("Gadar2",5.6f,150.00));
		al.add(new Movie("OMG2",4.5f,100.00));
		al.add(new Movie("De dhaka2",6.7f,200.00));
		System.out.println(al);
		Collections.sort(al,new SortByName());
		System.out.println(al);
		Collections.sort(al,new SortBySal());
		System.out.println(al);
		Collections.sort(al, newSortByIMDB());
		System.out.println(al);

	}
}



