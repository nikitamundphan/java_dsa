import java.util.*;
class MyClass{
	String str=null;
	MyClass(String str){
		this.str=str;
	}
	public String getName(){
		return str;
	}
}
class Demo implements Comparator{
	public int compare(MyClass obj1,MyClass obj2){
		return obj1.getName().compareTo(obj2.getName());
	}
}

class TreeDemo{
	public static void main(String[] args){
		TreeSet ts = new TreeSet();
		ts.add(new MyClass("Kanha"));
		ts.add(new MyClass("Ashish"));
		ts.add(new MyClass("Rahul"));
		ts.add(new MyClass("Badhe"));
		System.out.println(ts);
	}
}
