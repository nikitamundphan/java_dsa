import java.util.concurrent.*;
class BlockingQueueDemo{
	public static void main(String[] args)throws InterruptedException{
		BlockingQueue bq =new PriorityBlockingQueue(15);
		bq.offer(10);
		bq.offer(20);
		bq.offer(30);
		bq.put(40);
		bq.put(50);
		bq.put(60);
		bq.put(70);
		bq.put(80);
		bq.put(90);
		bq.put(100);
		bq.put(110);
		bq.put(120);
		bq.put(130);
		bq.put(140);
		bq.put(150);
		System.out.println(bq);
		bq.put(160);
		System.out.println(bq);
		

	}
}
