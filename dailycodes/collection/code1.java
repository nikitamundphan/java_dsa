import java.util.*;
class ArrayListDemo extends ArrayList{
	public static void main(String[] args){
		ArrayListDemo al =new ArrayListDemo();
		al.add(10);
		al.add(20.5f);
		al.add("Nikita");
		al.add(10);
		al.add(20.5f);
		System.out.println(al);
		al.add(3,"Core2web");
		System.out.println(al);
		System.out.println(al.size());
		System.out.println(al.contains("Nikita"));
		System.out.println(al.contains(30));
		System.out.println(al.indexOf(10));
		System.out.println(al.lastIndexOf(20.5f));
		System.out.println(al.get(3));
		System.out.println(al.set(3,"Mundphan"));
		System.out.println(al);


		ArrayList al2 =new ArrayList();
		al2.add("salman");
		al2.add("shahrukh");
		al2.add("amir");

		al.addAll(al2);
		System.out.println(al);

		al.addAll(3,al2);
		System.out.println(al);

		al.removeRange(3,5);
		System.out.println(al);

		System.out.println(al.remove(4));

		Object arr[] = al.toArray();
		for(Object data : arr){
			System.out.print(data+" ");
		}
		System.out.println();

		al.clear();
		System.out.println(al);
	}
}


		
