import java.util.*;
class SortDemo{
	public static void main(String[] args){
		SortedSet ss = new TreeSet();
		ss.add("Kanha");
		ss.add("Ashish");
		ss.add("Rahul");
		ss.add("Badhe");
		ss.add("Shashi");
		System.out.println(ss);
		System.out.println(ss.headSet("Kanha"));
		System.out.println(ss.tailSet("Kanha"));
		System.out.println(ss.subSet("Ashish","Shashi"));
		System.out.println(ss.first());
		System.out.println(ss.last());

	}
}


