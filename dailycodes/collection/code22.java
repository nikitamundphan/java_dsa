import java.util.*;
class Emp{
	String empName=null;
	float sal= 0.0f;
	Emp(String empName,float sal){
		this.empName=empName;
		this.sal= sal;
	}
	public String toString(){
		return "{" +empName+ ":"+sal+"}";
	}
}
class SortByName implements Comparator<Emp> {
	public int comapre(Emp obj1,Emp obj2){
		return obj1.empName.compareTo(obj2.empName);
	}
}
class SortBySal implements Comparator<Emp>{
	public int compare(Emp obj1, Emp obj2){
		return (int)(obj1.sal-obj2.sal);
	}
}
class ListDemo{
	public static void main(String[] args){
		ArrayList<Emp>al= new ArrayList<Emp>();
		al.add(new Emp("kanha",200000.00f));
		al.add(new Emp("rahul",250000.00f));
		al.add(new Emp("ashish",175000.00f));
		al.add(new Emp("badhe",100000.00f));
		System.out.println(al);
		Collections.sort(al,new SortByName());
		System.out.println(al);
		Collections.sort(al,new SortBySal());
		System.out.println(al);
	}
}



