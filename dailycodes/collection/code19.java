import java.util.*;
class Movies implements Comparable{
	String movName=null;
	float totcoll=0;
	Movies(String movName,float totcoll){
		this.movName=movName;
		this.totcoll=totcoll;
	}
	public int compareTo(Object obj){
		return (movName.compareTo(((Movies)obj).movName));
	}
	public String toString(){
		return movName;
	}
}
class SortListDemo{
	public static void main(String[] args){
		TreeSet ts=new TreeSet();
		ts.add(new Movies("gadar2",150.00f));
		ts.add(new Movies("omg2",120.00f));
		ts.add(new Movies("jailer",250.00f));
		System.out.println(ts);
	}
}

