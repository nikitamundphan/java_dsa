//queue implementation using array
import java.util.*;
class QueueDemo{
	int queueArr[];
	int rear;
	int front;
	int maxsize;
	QueueDemo(int size){
		this.queueArr=new int[size];
		this.front=-1;
		this.rear=-1;
		this.maxsize=size;
	}

	void enqueue(int data){
		if(rear==maxsize-1){
			System.out.println("queue is full");
		}
		else if(front ==-1 && rear==-1){
			front =0;
			rear=0;
			queueArr[rear]=data;
		}
		else{
			rear++;
			queueArr[rear]=data;
		}
	}
	int dequeue(){
		if(front==-1){
			System.out.println("queue is empty");
			return -1;
		}
		else{
			int val =queueArr[front];
			front++;
			if(rear<front){
				rear=front=-1;
			}
			return val;
		}
	}
	boolean empty(){
		if(front==-1){
			return true;
		}
		else{
			return false;
		}
	}
	int front(){
		if(front==-1){
			System.out.println("queue is empty");
			return -1;
		}
		else{
			return queueArr[front];
		}
	}
	int rear(){
		if(rear==-1){
			System.out.println("queue is empty");
			return -1;
		}
		else{
			return queueArr[rear];
		}
	}
	void printqueue(){
		if(front==-1){
			System.out.println("queue is empty");
		}
		else{
			for(int i=front;i<=rear;i++){
				System.out.print(queueArr[i]);
			}
			System.out.println();
		}
	}

}
class Client{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the array size");
		int size=sc.nextInt();
		QueueDemo q =new QueueDemo(size);
		char ch;
		do{
			System.out.println("1.enqueue");
			System.out.println("2.dequeue");
			System.out.println("3.empty");
			System.out.println("4.front");
			System.out.println("5.rear");
			System.out.println("6.printqueue");
			System.out.println("enter your choice");
			int choice=sc.nextInt();
			switch(choice){
				case 1:
					{
						System.out.println("enter the data for queue");
						int data=sc.nextInt();
						q.enqueue(data);
					}
					break;

				case 2:
					{
						int ret=q.dequeue();
						if(ret!=-1){
							System.out.println("popped"+ret);
						}
					}
					break;
				case 3:
					{
						boolean ret= q.empty();
						if(ret){
							System.out.println("quque is empty");
						}
					}
					break;

				case 4:{

						int ret=q.front();
							if(ret!=-1){
								System.out.println("front element is" +ret);
						}
				}

					break;

				case 5:
					{
						int ret=q.rear();
						if(ret!=-1){
							System.out.println("rear element " +ret);
						}
					}
					break;

				case 6:
					q.printqueue();
					break;

				default:
					System.out.println("wrong choice");
					break;

			}
			System.out.println("do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
	}
}


