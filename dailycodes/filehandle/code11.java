import java.io.*;
class Player implements Serializable{
	int jerno=0;
	String pName;
	Player(int jerno,String pName){
		this.jerno=jerno;
		this.pName=pName;
	}
}
class Demo{
	public static void main(String[] args)throws IOException{
		Player obj1=new Player(1,"klrahul");
		Player obj2= new Player(18,"virat kohli");
		FileOutputStream fos =new FileOutputStream("PlayerData.txt");
		ObjectOutputStream oos =new ObjectOutputStream(fos);
		oos.writeObject(obj1);
		oos.writeObject(obj2);
		oos.close();
		oos.close();
	}
}

