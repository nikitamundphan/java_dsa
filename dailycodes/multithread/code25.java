import java.util.concurrent.*;
class MyThread implements Runnable{
	int num;
	MyThread(int num){
		this.num=num;
	}
	public void run(){
		System.out.println(Thread.currentThread()+"start thread"+num);
		DailyTask();
		System.out.println(Thread.currentThread()+"end thread"+num);

	}
	void DailyTask(){
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}
class ThreadPoolDemo{
	public static void main(String[] args){
		ExecutorService ser=Executors.newSingleThreadExecutor();
		for(int i=0;i<=6;i++){
			MyThread obj = new MyThread(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}

