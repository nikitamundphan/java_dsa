class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}

	}
}
class ThreadGroupDemo{
	public static void main(String[] args)throws InterruptedException {
		ThreadGroup pthreadGP=new ThreadGroup("India");
		MyThread t1=new MyThread(pthreadGP,"maha");
		MyThread t2=new MyThread(pthreadGP,"goa");
		t1.start();
		t2.start();

		ThreadGroup cthreadGP=new ThreadGroup("Bangaladesh");
		MyThread t3=new MyThread(cthreadGP,"manipur");
		MyThread t4=new MyThread(cthreadGP,"Dhaka");
		t3.start();
		t4.start();
		System.out.println(pthreadGP.activeCount());
		System.out.println(pthreadGP.activeGroupCount());
	}
}

