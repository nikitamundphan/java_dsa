class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadGroupDemo{
	public static void main(String [] args){
		ThreadGroup pthreadGP=new ThreadGroup("Core2web");
		MyThread obj=new MyThread(pthreadGP,"c");
		MyThread obj1=new MyThread(pthreadGP,"java");
		MyThread obj2=new MyThread(pthreadGP,"python");
		obj.start();
		obj1.start();
		obj2.start();
	}
}



