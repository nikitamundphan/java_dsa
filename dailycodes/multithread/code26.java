import java.util.concurrent.*;
class MyThread implements Runnable{
	int num=0;
	MyThread(int num){
		this.num=num;
	}
	public void run(){
		System.out.println("start thread"+num+Thread.currentThread());
		dailyTask();
		System.out.println("end thread"+num+Thread.currentThread());
	}
	void dailyTask(){
		try{
			Thread.sleep(4000);
		}catch(InterruptedException ie){
		}
	}
}
class ThreadPoolDemo{
	public static void main(String[] args){
		ThreadPoolExecutor tpe = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		for(int i=1;i<=9;i++){
			MyThread obj=new MyThread(i);
			tpe.execute(obj);
		}
		tpe.shutdown();
	}
}
