class ArrayDemo{
	public static void main(String[] args){
		int arr[]=new int[4];
		arr[0]=10;
		arr[1]=20;
		arr[2]=30;
		arr[3]=40;
		int arr2[]={20,30,50};
		int arr3[]=new int[]{2,3,4,5};
		int arr4[]=new int[3]{4,5,6};
		for(int i=0;i<4;i++){
			System.out.println(arr[i]);
		}
		for(int i=0;i<3;i++){
			System.out.println(arr2[i]);
		}
		for(int i=0;i<4;i++){
			System.out.println(arr3[i]);
		}
		for(int i=0;i<3;i++){
			System.out.println(arr4[i]);
		}
	}
}


