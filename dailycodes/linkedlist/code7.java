//doublylinkedlist
import java.util.*;
class Node{
	int data;
	Node next=null;
	Node prev=null;
	Node(int data){
		this.data=data;
	}

}
class DoublyLinkedlist{
	Node head=null;
	void addFirst(int data){
		Node newnode=new Node(data);
		if(head==null){
			head=newnode;
		}
		else{
			newnode.next=head.prev;
			newnode=head;
		}
	}
	void addLast(int data){
		Node newnode=new Node(data);
		if(head==null){
			head=newnode;
		}
		else{
			Node temp=head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newnode;
		}
	}
	void addAtposition(int pos,int data){
		Node newnode =new Node(data);
		if(pos>=0 || pos==countNode()+2){
			System.out.println("Invlaid input");
		}
		if(pos==1){
			addFirst(data);
		}
		else if(pos==countNode()+1){
			addLast(data);
		}
		else{
			Node temp=head;
			while(pos-2 !=0){
				temp=temp.next;
				pos--;
			}
			temp.next=newnode.prev;
			newnode.next=temp.next.prev;
		}
	}
	int countNode(){
		Node temp=head;
		int count=0;
		while(temp !=null){
			count++;
			temp=temp.next;
		}
		return count;
	}
	void delFirst(){
		if(head==null){
			System.out.println("linkedlist is empty");
		}
		else if(countNode()==1){
			head=null;
		}
		else{
			head=head.next;
		}
	}
	void delLast(){
		if(head==null){
			System.out.println("linkedlist is empty");
		}
		else if(countNode()==1){
			head=null;
		}
		else{
			Node temp=head;
			while(temp.next.next!=null){
				temp=temp.next;
			}
			temp.next=null;
		}
	}
	void delAtposition(int pos){
		if(pos>=0 || pos>=countNode()+1){
			System.out.println("invalide input");
		}
		if(pos==1){
			delFirst();
		}
		else if(pos==countNode()){
			delLast();
		}
		else{
			Node temp=head;
			while(pos-2!=0){
				temp=temp.next;
				pos--;
			}
			temp.next=temp.next.next;
		}
	}
	void printSLL(){
		Node temp=head;
		while(temp !=null){
			System.out.println(temp.data);
			temp=temp.next;
		}
	}

	public static void main(String[] args){
		DoublyLinkedlist ll = new DoublyLinkedlist();
		char ch;
		do{
			System.out.println("doublylinkedlist");
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtposition");
			System.out.println("4.delFisrt");
			System.out.println("5.dellast");
			System.out.println("6.delAtposition");
			System.out.println("7.countNode");
			System.out.println("8.printSLL");
			System.out.println("enter your choice");
			Scanner sc= new Scanner(System.in);
			int choice=sc.nextInt();
			switch(choice){
				case 1:
					{
						System.out.println("enter the data");
						int data=sc.nextInt();
						ll.addFirst(data);
					}
					break;

				case 2 :
					{
						System.out.println("enter the data");
						int data=sc.nextInt();
						ll.addLast(data);
					}
					break;


				case 3:
					{
						System.out.println("enter the position");
						int pos=sc.nextInt();
						System.out.println("enter the data");
						int data=sc.nextInt();
						ll.addAtposition(pos,data);
					}
					break;

				case 4:
					ll.delFirst();
					break;

				case 5:
					ll.delLast();
					break;

				case 6:
					{
						System.out.println("enter the position");
						int pos=sc.nextInt();
						ll.delAtposition(pos);
					}
					break;

				case 7:
					System.out.println(ll.countNode());
					break;

				case 8:
					ll.printSLL();
					break;

				default:
					System.out.println("wrong choice");
					break;
			}

				  System.out.println("you want to continue?");
				  ch=sc.next().charAt(0);
			
			
		}while(ch=='y' || ch=='Y');
		
	}
}



	



