//singlylinkedlist
import java.util.*;
class Node{
	int data;
	Node next=null;
	Node(int data){
		this.data=data;
	}

}
class Linkedlist{
	Node head=null;
	void addFirst(int data){
		Node newnode=new Node(data);
		if(head==null){
			head=newnode;
		}
		else{
			newnode.next=head;
			newnode=head;
		}
	}
	void reverseList(){
		Node curr=head;
		Node prev=null;
		Node next=null;
		while(curr!=null){
			next=curr.next;
			curr.next=prev;
			prev=curr;
			curr=next;
		}
		head=prev;
		
	}
	void printSLL(){
		Node temp=head;
		while(temp !=null){
			System.out.println(temp.data);
			temp=temp.next;
		}
	}

	public static void main(String[] args){
		Linkedlist ll = new Linkedlist();
		char ch;
		do{
			System.out.println("Singly linkedlist");
			System.out.println("1.addFirst");
			System.out.println("2.reverseList");
			System.out.println("3.printSLL");
			System.out.println("enter your choice");
			Scanner sc= new Scanner(System.in);
			int choice=sc.nextInt();
			switch(choice){
				case 1:
					{
						System.out.println("enter the data");
						int data=sc.nextInt();
						ll.addFirst(data);
					}
					break;

				case 2 :
					
						ll.reverseList();
						break;
					

				case 3:
					ll.printSLL();
					break;

				default:
					System.out.println("wrong choice");
					break;
			}

				  System.out.println("you want to continue?");
				  ch=sc.next().charAt(0);
			
			
		}while(ch=='y' || ch=='Y');
		
	}
}



	



