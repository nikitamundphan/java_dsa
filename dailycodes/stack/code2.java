import java.util.*;
class StackDemo{
	int maxsize;
	int top=-1;
	int stackArr[];
	StackDemo(int size){
		this.stackArr=new int[size];
		this.maxsize=size;
	}
	void push(int data){
		if(top == maxsize-1){
			System.out.println("stack overflow");
			return;
		}
		else{
			top++;
			stackArr[top]=data;
		}
	}
	boolean empty(){
		if(top==-1){
			return true;
		}
		else{
			return false;
		}
	}
	int pop(){
		if(empty()){
			System.out.println("stack is empty");
			return -1;
		}
		else{
			int val=stackArr[top];
			top--;
			return val;
		}
	}
	int peek(){
		if(empty()){
			System.out.println("stack is empty");
			return -1;
		}
		else{
			return stackArr[top];
		}
	}
	int size(){
		return top;
	}
	void printStack(){
		if(empty()){
			System.out.println("nothing to print");

		}
		else{
			System.out.print("[");
			for(int val=0;val<=top;val++){
				System.out.print(stackArr[val]+" ");
			}
			System.out.println("]");
		}
		}
}
class client{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("enter the stack size");
		int size=sc.nextInt();
		StackDemo s =new StackDemo(size);
		char ch;
		do{

		        System.out.println("enter your choice");
	         	int choice=sc.nextInt();
	
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.peek");
			System.out.println("4.empty");
			System.out.println("5.size");
			System.out.println("6.printStack");

			switch(choice){
				case 1:
					{
						System.out.println("enter the data for stack");
						int data= sc.nextInt();
						s.push(data);
					}
					break;
				case 2:
					{
						int flag = s.pop();
						if(flag!=-1){
							System.out.println(flag + "popped");
						}
					}
					break;

				case 3:
					{
						int flag = s.peek();
						if(flag!=-1){
							System.out.println(flag);
						}
					}
					break;

				case 4:
					{
					boolean val=s.empty();
					if(val)
						System.out.println("stack is empty");
					else
						System.out.println("not empty");
					}
					break;
				case 5:
					int sz=s.size();
					System.out.println("size of stack"+(sz+1));
					break;
				case 6:
					s.printStack();
					break;
				default :
					System.out.println("wrong choice");
					break;
			}
			System.out.println("do you want to continue?");
			 ch =sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
	}
}



