//implementation of stack using linklist
import java.util.*;
class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}
class Stack{
	Node top=null;
	void push(int data){
		Node newnode=new Node(data);
		if(top==null){
			newnode=top;
		}
		else{
			newnode.next=top;
			top=newnode;
		}
	}
	int pop(){
		if(top==null){
			System.out.println("stack is empty");
			return -1;
		}
		else{
			int val=top.data;
			top=top.next;
			return val;
		}
	}
	void peek(){
		if(top==null){
			System.out.println("stack is empty");
		}
		else{
			System.out.println(top.data);
		}
	}
	void printStack(){
		if(top==null){
			System.out.println("nothing to print");
		}
		else{

			Node temp=top;
			while(top.next!=null){
				System.out.println(temp.data);
		}
	}
	}
}
class Client{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		Stack s= new Stack();
		System.out.println("enter the size of stack");
		int size=sc.nextInt();
		char ch;
		do{
			System.out.println("enter your choice");
			int choice=sc.nextInt();
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.peek");
			System.out.println("4.printStack");

			switch(choice){
				case 1:
					{
						for(int i=0;i<size;i++){

							System.out.println("enter data for stack");
							int data=sc.nextInt();
							s.push(data);
						}

					}
					break;
				case 2:
					{
						int flag=s.pop();
						if(flag!=-1){
							System.out.println(flag +"popped");
						}
					}
					break;
				case 3:
					s.peek();
					break;
				case 4:
					s.printStack();
					break;
				default:
					System.out.println("wrong choice");
					break;
			}
			System.out.println("do you want to continue?");
			ch=sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
	}
}






