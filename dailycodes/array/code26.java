import java.io.*;
class Jagged{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the row:");
		int row=Integer.parseInt(br.readLine());
		int arr[][]=new int[row][];
		int a=Integer.parseInt(br.readLine());
		int b=Integer.parseInt(br.readLine());
		int c=Integer.parseInt(br.readLine());
		arr[0]=new int[a];
		arr[1]=new int[b];
		arr[2]=new int[c];
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}
		for(int [] x:arr){
			for(int y:x){
				System.out.print(y+"  ");
			}
			System.out.println();
		}
	}
}

