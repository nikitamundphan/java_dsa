class Stringdemo{
	public static void main(String[] args){
		String str1="Nikita";
		String str2=new String("Nikita");
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		String str3=new String("Nikita");
		String str4="Nikita";
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		String str5="Sakshi";
		System.out.println(System.identityHashCode(str5));
	}
}
