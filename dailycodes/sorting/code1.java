//Bubble sort
class Bubblesort{
	public static void main(String[] args){
		int arr[]=new int[]{2,7,9,1,3,5,6,4};
		int count=0;
		boolean swap;
		for(int i=0;i<arr.length;i++){
			swap=false;
			for(int j=0;j<arr.length-i-1;j++){
				count++;
				if(arr[j]>arr[j+1]){
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
					swap=true;
				}
			}

		        if(swap==false){
				break;
			}
		}
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	
	}
}


