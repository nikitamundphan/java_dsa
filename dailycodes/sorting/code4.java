//selection sort
class Selectionsort{
	void sort(int arr[]){
		int n=arr.length;
		for(int i=0;i<n-1;i++){
			int min=i;
			for(int j=i+1;j<n;j++){
				if(arr[j]<arr[min]){
					min=j;
				}
			}
			int temp=arr[min];
			arr[min]=arr[i];
			arr[i]=temp;
		}
	}
	public static void main(String[] args){
		int arr[]=new int[]{9,6,8,2,5,3,7};
		Selectionsort obj=new Selectionsort();
		obj.sort(arr);
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}

