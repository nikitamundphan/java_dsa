//bubblesort by using recurssion
class Recursion{
	static void bubblesort(int arr[],int n){
		if(n==1){
			return;
		}
		for(int j=0;j<arr.length-1;j++){
			if(arr[j]>arr[j+1]){
				int temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
		}
		bubblesort(arr,n-1);
	}
	public static void main(String[] args){
		int arr[]=new int[]{7,3,9,4,2,5,6};
		int n=arr.length;
		bubblesort(arr,n);
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}

}


