//merge sort
import java.util.*;
class Mergesort{
	void merge(int arr[],int start,int mid,int end){
		int n1=mid-start+1;
		int n2=end-mid;
		int arr1[]=new int[n1];
		int arr2[]=new int[n2];
		for(int i=0;i<n1;i++){
			arr1[i]=arr[start+i];
		}
		for(int j=0;j<n2;j++){
			arr2[j]=arr[mid+1+j];
		}
		int i=0,j=0;
		int k=1;
		while(i<n1 && j<n2){
			if(arr1[i]<arr2[j]){
				arr[k]=arr1[i];
				i++;
			}
			else{
				arr[k]=arr2[j];
				j++;
			}
			k++;
		}
		while(i<n1){
			arr[k]=arr1[i];
			i++;
			k++;
		}
		while(j<n2){
			arr[k]=arr2[j];
			j++;
			k++;
		}
	}
	void mergesort(int arr[],int start,int end){
		if(start<end){
			int mid = start+(end-start)/2;
	
		mergesort(arr,start,mid);
		mergesort(arr,mid+1,end);
		merge(arr,start,mid,end);
	}
	}

	public static void main(String[] args){
		int arr[]=new int[]{12,11,13,5,6,7};
		Mergesort obj=new Mergesort();
		int start=0;
		int end=arr.length-1;
		obj.mergesort(arr,start,end);
		System.out.println(Arrays.toString(arr));
	}
}

	



