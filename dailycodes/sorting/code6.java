//Quicksort
import java.util.*;
class Quicksort{
	void quicksort(int arr[],int start,int end){
		if(start<end){
			int pivotindex=partition(arr,start,end);
			quicksort(arr,start,pivotindex-1);
			quicksort(arr,pivotindex+1,end);
		}
	}
	int partition(int arr[],int start,int end){
		int i=start-1;
		int pivot=arr[end];
		for(int j=0;j<end;j++){
			if(arr[j]<pivot){
				i++;
				int temp = arr[j];
				arr[j]=arr[i];
				arr[i]=temp;
			}
		}
		i++;
		int temp=arr[i];
		arr[i]=arr[end];
		arr[end]=temp;
		return i;
	}
	public static void main(String[] args){
		int arr[]=new int[]{12,7,6,14,5,15,10};
		int start=0;
		int end=arr.length-1;
		Quicksort obj= new Quicksort();
		obj.quicksort(arr,start,end);
		System.out.println(Arrays.toString(arr));
	}
}


