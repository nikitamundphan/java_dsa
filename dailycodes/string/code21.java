import java.io.*;
class LengthDemo{
	static int mystrlen(String str){
		char arr[]=str.toCharArray();
		int count=0;
		for(int i=0;i<arr.length;i++){
			count++;
		}
		return count;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the string");
		String str=br.readLine();
		int n=mystrlen(str);
		System.out.println(n);
	}
}
