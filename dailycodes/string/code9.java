class HashDemo{
	public static void main(String[] args){
		String str1="Nikita";
		String str2=new String("Mundphan");
		String str3="Nikita";
		String str4=new String("Nikita");
		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}
}
