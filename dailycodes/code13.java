// print fizz if divisible by 3
// print buzz if divisible by 5
// print fizz-buzz if divisible by both
// if not the print not divisible by both
class Input{
	public static void main(String[]args){
		int num=15;
		if(num%3==0 && num%5==0){
			System.out.println("fizz-buzz");
		}
		else if(num%3==0){
			System.out.println("fizz");
		}
		else if(num%5==0){
			System.out.println("buzz");
		}
		else{
			System.out.println("not divisible by both");
		}
	}

}
