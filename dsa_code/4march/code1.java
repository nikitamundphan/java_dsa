//buy and sell stock
import java.util.*;

class Solution {
    public int maxProfit(int[] prices) {
        int min = prices[0], n = prices.length;
    int[] dp = new int[n];
    //l->r
    for(int i = 1; i < n; i++){
        dp[i] = Math.max(dp[i - 1], prices[i] - min);
        if(min > prices[i]) min = prices[i];
    }
    int result = dp[n - 1];
    int max = prices[n - 1], prev = 0;
    //r->l
    for(int i = n - 2; i > -1; i--){
        prev = Math.max(prev, max - prices[i]);
        result = Math.max(result, prev + dp[i]);
        if(max < prices[i]) max=  prices[i];
    }
    return result;

    }
}
