//max consecutive one
class Solution{
	int maccount(int arr[],int n){
		int count1=0;
		int count2=0;
		for(int i=0;i<n-1;i++){
			if(arr[i]==arr[i+1]==1){
				count1++;
			}
		}
		return count1;
	}
}
class client{
	public static void main(String[] args){
		int arr[]=new int[]{1,1,0,1,1,1};
		int n=arr.length;
		Solution s= new Solution();
		int ret=s.maxcount(arr,n);
		System.out.println(ret);
	}
}
