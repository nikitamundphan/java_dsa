//rotate 2d array without using extra space

class Solution {
    void rotateMatrix(int arr[][], int n) {
        int a=0,b=0,c=0,d=0;
        for(int i=0;i<=n/2-1;i++){
            for(int j=0;j<=n-2*i-2;j++){
                a=arr[i+j][i];
                b=arr[n-1-i][i+j];
                c=arr[n-1-i-j][n-1-i];
                d=arr[i][n-1-i-j];
                arr[i+j][i]=d;
                arr[n-1-i][i+j]=a;
                arr[n-1-i-j][n-1-i]=b;
                arr[n-1-i-j]=c;
            }
            
        }
    }
}
