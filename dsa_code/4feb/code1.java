//maximum difference
import java.util.*;
class Solution{
	static int maxdiff(int nums[]){
		Arrays.sort(nums);
		int max=0;
		for(int i=1;i<nums.length;i++){
			if(max<nums[i]-nums[i-1]){
				max=nums[i]-nums[i-1];
			}
		}
		return max;
	}
	public static void main(String[] args){
		int nums[]=new int[]{1,2,3,4};
		int ret=maxdiff(nums);
		System.out.println(ret);
	}
}

