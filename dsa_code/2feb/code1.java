//product of array itself
class Solution{
	static int[]product(int[] nums){
		int prefixproduct=1;
		int suffixproduct=1;
		int n=nums.length;
		int result[]=new int[nums.length];
		for(int i=0;i<n;i++){
			result[i]=prefixproduct;
			prefixproduct=prefixproduct*nums[i];
		}
		for(int i=n-1;i>=0;i--){
			result[i]=suffixproduct;
			suffixproduct=suffixproduct*nums[i];
		}
		return result;
	}
	public static void main(String[] args){
		int nums[]=new int[]{1,2,3,4};
		int arr[]=product(nums);
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]);
		}
		System.out.println();
	}
}


