//number of zero-filled subaarays
class Solution{
	public static long zerofilled(int []nums){
		int len=0;
		int sum=0;
		long count=0;
		for(int i=0;i<nums.length;i++){
			sum=sum+nums[i];
			if(sum!=0){
				sum=0;
				len=0;
				continue;
			}
			len++;
			count=count+len;
		}
		return count;
	}
	public static void main(String[] args){
		int nums[]=new int[]{3,2,0,0,4,5,0,0};
		long ret=zerofilled(nums);
		System.out.println(ret);
	}
}

