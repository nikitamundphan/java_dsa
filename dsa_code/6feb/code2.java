//pairs of songs with total duration divisible by 60
class Solution{
	 public static int numPairsDivisibleBy60(int[] time) {
        int count=0;
        for(int i=0;i<time.length;i++){
            for(int j=1;j<time.length;j++){
                if(time[i]+time[j]%60==0){
                    count++;
                }

        }
        }
        return count;

    }
    public static void main(String[] args){
	    int time[]={30,50,150,120};
	    int ret=numPairsDivisibleBy60(time);
	    System.out.println(ret);
    }
}



