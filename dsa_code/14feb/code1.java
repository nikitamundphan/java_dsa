//3sum closest
import java.util.*;
class Solution {
    public static int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        int closesum=nums[0]+nums[1]+nums[2];
        for(int i=0;i<nums.length;i++){
            int left=i+1;
            int right=nums.length-1;
            while(left<right){
                int sum=nums[i]+nums[left]+nums[right];
                if(Math.abs(sum-target)<Math.abs(closesum-target)){
                    closesum=sum;
                }
                if(sum<target){
                    left++;
                }
                else{
                    right--;
                }
            }
        }
        return closesum;

    }
    public static void main(String[] args){
	    int nums[]={1,2,3,4};
	    int target=2;
	    int ret=threeSumClosest(nums,target);
	    System.out.println(ret);
    }
}


