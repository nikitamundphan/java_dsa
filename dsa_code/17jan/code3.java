//leaders in an array
import java.util.*;
class Solution{
	ArrayList<Integer>leader(int arr[],int n){
		ArrayList<Integer>res =new ArrayList<Integer>();
		int max=arr[n-1];
		for(int i=n-1;i>=0;i--){
			if(arr[i]>max){
				max=arr[i];
				res.add(max);
		}
	}
	Collections.reverse(res);
	return res;
	}

}
class Client{
	public static void main(String[] args){
		int arr[]=new int[]{17,1,3,5,15,2};
		int n=arr.length;
		ArrayList<Integer>ret=new ArrayList<Integer>();
		Solution s= new Solution();
		ret=s.leader(arr,n);
		for(int i=0;i<ret.length;i++){
			System.out.println(ret[i]);
		}
	}
}



