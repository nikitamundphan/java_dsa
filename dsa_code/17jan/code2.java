//count pair with given sum
class Solution{
	int count(int arr[],int n,int k){
		int count=0;
		for(int i=0;i<n;i++){
			int j=0;
			for (j=i+1;j<n;j++){
				if(arr[i]+arr[j]==k){
					count++;
				}
			}
		}
		return count;
	}
	
}

class Client{
	public static void main(String[] args){
		int arr[]=new int[]{1,5,7,1};
		int n=arr.length;
		int k=6;
		Solution s= new Solution();
		int ret=s.count(arr,n,k);
		System.out.println(ret);
	}
}


