//majority element
import java.util.*;
class Solution{
	int majority(int arr[],int n){
		Arrays.sort(arr);
		return arr[n/2];
	}
}
class Client{
	public static void main(String[] args){
		int arr[]=new int[]{3,2,3};
		int n=arr.length;
		Solution s= new Solution();
		int ret=s.majority(arr,n);
		System.out.println(ret);
	}
}

