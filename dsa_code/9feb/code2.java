//minimize the hieghts
import java.util.*;
class Solution{
    public static int getdiff(int n,int k,int arr[]){
	Arrays.sort(arr);
        // Maximum possible height difference
        int ans = arr[n - 1] - arr[0];

        int tempmin, tempmax;
        tempmin = arr[0];
        tempmax = arr[n - 1];

        for (int i = 1; i < n; i++) {

            // if on subtracting k we got negative then
            // continue
            if (arr[i] - k < 0)
                continue;

            // Minimum element when we add k to whole array
            tempmin = Math.min(arr[0] + k, arr[i] - k);

            // Maximum element when we subtract k from whole
            // array
            tempmax
                = Math.max(arr[i - 1] + k, arr[n - 1] - k);
            ans = Math.min(ans, tempmax - tempmin);
        }
        return ans;
    }
    public static void main(String[] args){
	    int arr[]={1,2,3,4,5,8};
	    int n=arr.length;
	    int k=2;
	    int ret=getdiff(n,k,arr);
	    System.out.println(ret);
    }
}


