//container with most water
import java.util.*;
class Solution{
	public static int maxArea(int height[]){
		int max=0;
		int j=height.length-1;
		int i=0;
		while(i<j){
			int l=j-i;
			int h=0;
			if(height[j]>height[i]){
				h=height[i];
				i++;
			}
			else{
				h=height[j];
				j--;
			}
			int area=l*h;
			if(area>max){
				max=area;
			}
		}
		return max;
	}
	public static void main(String[] args){
		int height[]={1,2,3,5,7};
		int ret=maxArea(height);
		System.out.println(ret);
	}
}

