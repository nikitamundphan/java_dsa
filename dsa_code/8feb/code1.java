//task sheduler
import java.util.*;
class Solution{
	public static int leastinterval(int tasks[],int n){
		 if (n == 0 || tasks.length == 1) return tasks.length;

        int[] freq = count(tasks);

        Arrays.sort(freq);
        // place task with maximum priority first
        int max = freq[25]-1;
		int spaces = max * n;

		for (int i = 24; i >= 0; i--) {
            if (freq[i] == 0) break;
			spaces -= Math.min(max, freq[i]);
		}

		/* Handle cases when spaces become negative */
		spaces = Math.max(0, spaces);

		return tasks.length + spaces;
    }

    private static int[] count(char[] tasks) {
        int[] freq = new int[26];

        for (int i = 0; i < tasks.length; i++) {
            freq[tasks[i] - 'A']++;
        }

        return freq;
    }
    public static void main(String[] args){
	    char tasks[]={'A','A','A','A','B','B'};
	    int n=2;
	    int ret=leastinterval(tasks,n);
	    System.out.println(ret);
    }
}



