//maximize the score
import java.util.*;
class Solution{
	 static int maxProfit(int K, int N, int A[]) {
         int profit[][] = new int[K + 1][ N + 1];

        // For day 0, you can't earn money
        // irrespective of how many times you trade
        for (int i = 0; i <= K; i++)
            profit[i][0] = 0;

        // profit is 0 if we don't do any
        // transaction (i.e. k =0)
        for (int j = 0; j <= N; j++)
            profit[0][j] = 0;

        // fill the table in bottom-up fashion
        for (int i = 1; i <= K; i++)
        {
            int prevDiff = Integer.MIN_VALUE;
            for (int j = 1; j < N; j++)
            {
                prevDiff = Math.max(prevDiff,
                           profit[i - 1][j - 1] -
                           A[j - 1]);
                profit[i][j] = Math.max(profit[i][j - 1],
                               A[j] + prevDiff);
            }
        }

        return profit[K][N - 1];
	 }
}

