//median of two sorted array
import java.util.*;
class Solution {
    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int len1 = nums1.length;
        int len2 = nums2.length;
        int[] mergedArr = new int[len1 + len2];

        int i = 0, j = 0, k = 0;

        while (i < len1 && j < len2) {
            if (nums1[i] <= nums2[j]) {
                mergedArr[k++] = nums1[i++];
            } else {
                mergedArr[k++] = nums2[j++];
            }
        }

        while (i < len1) {
            mergedArr[k++] = nums1[i++];
        }

        while (j < len2) {
            mergedArr[k++] = nums2[j++];
        }

        int newLen = mergedArr.length;
        double ans = -1;

        if (newLen % 2 == 0) {
            int mid = newLen / 2;
            double res = (double) (mergedArr[mid - 1] + mergedArr[mid]) / 2;
            ans = res;
        } else {
            int mid = newLen / 2;
            ans = mergedArr[mid];
        }

        return ans;

        
    }
    public static void main(String[] args){
	    int nums1[]={1,2,3,4,5,7};
	    int nums2[]={8,9,5,6,3,10};
	    double ret=findMedianSortedArrays(nums1,nums2);
	    System.out.println(ret);
    }
}


