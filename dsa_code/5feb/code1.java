//sum of subarray equals to k
class Solution{
	static int subarray(int nums[],int k){
		 int count=0;
        
         for(int i=0;i<nums.length;i++){
             int sum=0;
             for(int j=i;j<nums.length;j++){
                sum+=nums[j];
                if(sum==k){
                    count++;
                }
             }
         }
         
         return count; 
	}
	public static void main(String[] args){
		int nums[]=new int[]{1,1,2};
		int k=2;
		int ret=subarray(nums,k);
		System.out.println(ret);
	}
}
