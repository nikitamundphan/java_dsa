//jump game
import java.util.*;
class Solution {
    public static  boolean canJump(int[] nums) {
        int reach=0;
        for(int i=0;i<nums.length;i++){
            if(i>reach){
                return false;
            }
            reach=Math.max(reach,i+nums[i]);

        }
        return true;
        
        
    }
    public static void main(String[] args){
	    int nums[]={1,2,3,4,5};
	    boolean ret=canJump(nums);
	    System.out.println(ret);
    }
}


