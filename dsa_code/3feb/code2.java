//find the duplicate number
import java.util.*;
class Solution{
	public static int duplicate(int nums[]){
		Arrays.sort(nums);
		for(int i=0;i<nums.length;i++){
			if(nums[i]==nums[i+1]){
				return nums[i];
			}
		}
		return 0;
	}
	public static void main(String[] args){
		int nums[]=new int[]{1,2,4,5,6,3,8,3};
		int ret=duplicate(nums);
		System.out.println(ret);
	}
}

