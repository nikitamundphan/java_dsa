//find all duplicates from array
import java.util.*;
class Solution{
	public List<Integer>findduplicates(int nums){
		ArrayList<Integer>arr=new ArrayList<>();
		Arrays.sort(nums);
		for(int i=1;i<nums.length;i++){
			if(nums[i-1]==nums[i]){
				arr.add(nums[i]);
			}
		}
		return arr;
	}
	public static void main(String[] args){
	      int nums[]=new int[]{1,1,2};
	      ArrayList<Integer>arr=findduplicate(nums);
	      for(int i=0;i<arr.length;i++){
		      System.out.println(arr[i]);
	      }
	}
}

