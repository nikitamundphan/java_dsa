//remove element
class Solution{
	int removeelement(int arr[],int num){
		int i=0;
		for(int j=0;j<arr.length;j++){
			if(arr[j]!=num){
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
				i++;
			}
		}
		return i;
	}
}
class Client{
	public static void main(String[] args){
		int arr[]=new int[]{1,5,6,8,9,3,4};
		int n=arr.length;
		int num=6;
		Solution s= new Solution();
		int ret=s.removeelement(arr,num);
		System.out.println(ret);
	}
}



