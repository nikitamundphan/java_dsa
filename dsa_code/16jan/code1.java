//two sum
class Solution{
	int []twosum(int arr[],int n,int target){
		int ans[]=new int[2];
		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length-1;j++){
				if(arr[i]+arr[j]==target){
					ans[0]=i;
					ans[1]=j;
					return ans;
				}
			}
		}
		return ans;
	}
}
class Client{
	public static void main(String[] args){
		int arr[]=new int[]{4,6,9,2,5,8};
		int n=arr.length;
		int target=10;
		Solution s= new Solution();
		int ret[]=s.twosum(arr,n,target);
		for(int i=0;i<ret.length;i++){
			System.out.print("["+ret[i]+ ",");
		}
		System.out.println("]");
	}
}



