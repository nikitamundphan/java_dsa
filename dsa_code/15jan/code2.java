//first reapeating element
class Solution{
	int firstrepeat(int arr[],int n){
		for(int i=1;i<n;i++){
		   for(int j=i+1;j<n;j++){
			   if(arr[i]==arr[j]){
				   return i+1;
			   }
		   }

		}
		return -1;
	}
}
class Client{
	public static void main(String[] args){
		int arr[]={1,5,3,6,5,7,3};
		int n=arr.length;
		Solution s= new Solution();
		int ret=s.firstrepeat(arr,n);
		System.out.println(ret);
	}
}

