//single element
import java.util.*;
class Solution{
	int single(int arr[]){
		int n=arr.length;
		Arrays.sort(arr);
		int i=0;
		while(i<n-2){
			if(arr[i]==arr[i+2]){
				i=i+2;
			}
			else{
				return arr[i];
			}
		}
		return arr[n-1];
	}
}
class Client{
	public static void main(String[]args){
		int n=5;
		int arr[]={1,2,3,4,1};
		Solution s= new Solution();
		int ret = s.single(arr);
		System.out.println(ret);
	}
}



