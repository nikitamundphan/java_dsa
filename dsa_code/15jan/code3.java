//key pair
import java.util.*;
class Solution{
	boolean checkpair(int arr[],int n,int x){
		Arrays.sort(arr);
		int left=0;
		int right=n-1;
		while(left<right){
			int currentsum=arr[left]+arr[right];
			if(currentsum==x){
				return true;
			}
			else if(currentsum<x){
				left++;
			}
			else{
				right--;
			}
		}
		return false;
	}
}
class Client{
	public static void main(String[] args){
		int arr[]={2,7,11,15};
		int n=4;
		int x=13;
		Solution s= new Solution();
		boolean ret=s.checkpair(arr,n,x);
		if(ret){
			System.out.println("yes");
		}
		else{
			System.out.println("no");
		}
	}
}


