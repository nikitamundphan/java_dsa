//count inversion
import java.util.*;
class Solution{
	class Solution
{
    // arr[]: Input Array
    // N : Size of the Array arr[]
    //Function to count inversions in the array.
    static long inversionCount(long arr[], long N)
    {
        long count=0;
        for(int i=0;i<N-1;i++){
            for(int j=i+1;j<N;j++){
                if(arr[i]>arr[j]){
                    count++;
                }
            }
        }
        return count;
    }
    public static void main(String[] args){
	    long arr[]={1,2,4,5,6};
	    int N=arr.length;
	    long ret=inversionCount(arr,N);
	    System.out.println(ret);
    }
}

