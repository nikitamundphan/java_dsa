//non overlap intervals
import java.util.*;
class Solution{
	public static int overlap(int intervals[][]){
		Arrays.sort(intervals,(a,b)->a[1]-b[1]);
        Stack<int[]> stk = new Stack<>();
        int res=0;
        for(int[] i : intervals){
            if(!stk.isEmpty() && i[0]< stk.peek()[1]){
                res++;
            }else{
                stk.push(i);
            }
        }

    return res;
	}
	public static void main(String[] args){
		int intervals[][]={{1,2,3},{4,5,6},{7,8,9}};
		int ret=overlap(intervals);
		System.out.println(ret);
	}
}

