//subarray division by k
import java.util.*;
class Solution {
    public int subarraysDivByK(int[] nums, int k) {
        int[] map=new int[k];
        int sum=0,c=0;
        map[0]=1;
        for(int i:nums){
            sum+=i;
            int r=sum%k;
            while(r<0) r+=k;
            c+=map[r];
            map[r]++;
        }
        return c;
    }
}

