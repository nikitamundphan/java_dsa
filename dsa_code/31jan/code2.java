//kth largest element 
import java.util.*;
class Solution{
	int kthlargest(int arr[],int k){
		int n=arr.length;
		Arrays.sort(arr);
		return arr[n-k];
	}
}
class Client{
	public static void main(String[] args){
		int arr[]=new int[]{2,3,7,9,6};
		int k=3;
		Solution s= new Solution();
		int ret=s.kthlargest(arr,k);
		System.out.println(ret);
	}
}



