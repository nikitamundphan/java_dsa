//sort the colors
class Solution{
	public static int[] sort(int nums[]){
		int n=nums.length;
		for(int i=0;i<n;i++){
			for(int j=i+1;j<n;j++){
				if(nums[i]>nums[j]){
					int temp=nums[i];
					nums[i]=nums[j];
					nums[j]=temp;
				}
			}
		}
		return nums;
	}
	public static void main(String[] args){
		int nums[]=new int[]{1,3,0};
		sort(nums);
		for(int i=0;i<nums.length;i++){
			System.out.print(nums[i]+",");
		}
		System.out.println();
	}
}


