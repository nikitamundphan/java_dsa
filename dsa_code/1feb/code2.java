//row with max 1's
class Solution{
	public static int[] max1s(int mat[][]){
		int n=mat.length;
		int m=mat[0].length;
		int ans[]=new int[2];
		int max=0;
		for(int i=0;i<n;i++){
			int count=0;
			for(int j=0;j<m;j++){
				if(mat[i][j]==1){
					count++;
				}
			}
			if(count>max){
				max=count;
				ans[0]=i;
				ans[1]=max;
			}
		}
		return ans;
	}
	public static void main(String[] args){
		int mat[][]={{0,1},{1,0}};
		int arr[]=max1s(mat);
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]);
		}
		System.out.println();
	}
}




