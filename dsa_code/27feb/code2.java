//count all subarray having sum divisible by k
import java.util.*;
class Solution
{
    long subCount(long arr[] ,int n,int k)
    {
        int sum=0;
        int count=0;
    HashMap<Integer,Integer>map=new HashMap<>();
    map.put(0,1);
     for(int i=0;i<n;i++)
     {
           sum+=arr[i];

           int div=sum%k;
           if(div<0)
           div+=k;

           if(map.containsKey(div))
           {
               count+=map.get(div);
           }
           map.put(div,map.getOrDefault(div,0)+1);
     }
     return count;


    }
}
