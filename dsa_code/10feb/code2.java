// kth element of sorted array
import java.util.*;
class Solution{
    public static long kthElement( int arr1[], int arr2[], int n, int m, int k) {
        int[] arr = new int[n+m];
        int i = 0, j = 0, d = 0;
        while (i < m && j <n)
        {
            if (arr1[i] < arr2[j])
                arr[d++] = arr1[i++];
            else
                arr[d++] = arr2[j++];
        }
        while (i < m){
            arr[d++] = arr1[i++];
        }
        while (j < n){ 
            arr[d++] = arr2[j++];
        }
        
		return (long)arr[k-1];
    }
 
    public static void main(String[] args){
	    int arr1[]={1,4,5,7,8};
	    int arr2[]={3,6,9};
	    int n=arr1.length;
	    int k=5;
	    int m=arr2.length;
	    long ret=kthElement(arr1,arr2,n,m,k);
	    System.out.println(ret);
    }
}


