//find median of all subarrays with particular size
import java.util.*;
class Solution{
	public static int median(int []v){
		Arrays.sort(v);
        int n=v.length;
        if(n%2==0){
            return (v[n/2]+v[n/2-1])/2;
        }
        else {
            return v[n/2];
        }
	}
	public static void main(String[] args){
		int v[]=new int[]{1,2,3,4,6,7,8};
		int ret=median(v);
		System.out.println(v);
	}

	}

