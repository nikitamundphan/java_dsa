//maximum subarray
import java.util.*;
class Solution{
	public static int maxarray(int nums[]){
	 int n = nums.length;
        int max = Integer.MIN_VALUE, sum = 0;

        for(int i=0;i<n;i++){
            sum += nums[i];
            max = Math.max(sum,max);

            if(sum<0) sum = 0;
        }

        return max;
	}
	public static void main(String[] args){
		int nums[]=new int[]{1,-2,3,4,6,9};
		int ret=maxarray(nums);
		System.out.println(ret);
	}
}

