//majority element Ii
import java.util.*;
class Solution{
     static List<Integer>major(int nums){
	List<Integer> ans=new ArrayList<>();
        Arrays.sort(nums);
        int count=1;
        for(int i=1; i<nums.length; i++){
            if(nums[i]==nums[i-1]){
                count++;
            }else{
                if(count>nums.length/3){
                    ans.add(nums[i-1]);
                }
                count=1;
            }
        }
        if(count>nums.length/3){
            ans.add(nums[nums.length-1]);
        }
        return ans;
     }
     public static void main(String[] args){
	     int nums[]={1,2,3,3};
	     List<Integer>ans=new List[nums.length];
	     ans=major(nums);
	     for(int i=0;i<ans.length;i++){
		     System.out.println(ans[i]);
	     }
     }
}



