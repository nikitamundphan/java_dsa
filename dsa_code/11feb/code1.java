//missing and repeating number
import java.util.*;
class Solution{
	static int[] findTwoElement(int arr[], int n) {
     int ans[]=new int[2];
     for(int i=0;i<n;i++){
         int val=Math.abs(arr[i]);
         if(arr[val-1]>0){
             arr[val-1]=-arr[val-1];
         }
         else{
             ans[0]=val;
         }
     }
     for(int i=0;i<n;i++){
         if(arr[i]>0){
             ans[1]=i+1;
         }
     }
     return ans;
	}
     public static void main(String[] args){
	     int arr[]=new int[]{1,2,3,4,6};
	     int n=arr.length;
	     int temp[]=findTwoElement(arr,n);
	     for(int i=0;i<temp.length;i++){
		     System.out.println(temp[i]);
	     }
     }
}


