//max digit in given number
class Demo{
	int max=Integer.MIN_VALUE;
	void digit(int num){
		if(num/10==0){
			return ;
		}
		if(num%10 >max){
			max=num%10;
		}
		digit(num/10);
	}
	public static void main(String[] args){
		Demo obj= new Demo();
		obj.digit(12345);
		System.out.println(obj.max);
	}
}

