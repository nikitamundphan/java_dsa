//number is strong or not
import java.util;
class Demo{
	boolean isstrong(int num,int temp){
		if(num==0){
			return temp=0;
		}
		else{
			int digit =num%10;
			int fact;
			if(digit ==0 || digit==1){
				fact=1;
			}
			else{
				fact = digit*calculateFactorial(digit-1);
			}
			return fact+ isstrong(num/10,temp);
		}
	}

	public static void main(String[] args){
		int num=153;
		if(isstrong(num)){
			System.out.println(num +"is a strong number");
		}
		else{
			System.out.println(num +"is not a strong number");
		}
	}
}

