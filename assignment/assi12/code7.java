//given number is magic or not
import java.util.*;

class Demo{
	static boolean ismagic(int num){
		if(num<=9){
			return num==1;
		}
		else{
			int sum=calculate(num);
			return ismagic(num);
		}
	}
	static int calculate(int num){
		if(num==0){
			return 0;
		}
		else{
			return num%10+calculate(num/10);
		}
	}
	public static void main(String[] args){
		int num=18;
		if(ismagic(num)){
			System.out.println(num +"is a magic number");
		}
		else{
			System.out.println(num +"is not a magic number");
		}
	}
}


