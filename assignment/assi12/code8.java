// number is perfect or not
class Demo{
	static boolean isperfect(int num,int div,int sum){
		if(div>num/2){
			return sum==num;
		}
		if(num%div==0){
			sum=sum+div;
		}
		return perfect(num,div+1,sum);
	}
	public static void main(String[] args){
		int num=234;
		if(isperfect(num)){
			System.out.println(num +"is a perfect number");
		}
		else{
			System.out.println(num+ "is not a perfect number");
		}
	}
}

