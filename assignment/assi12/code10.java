//number is armstrong or not 
import java.util.scanner;
class Demo{
	static int armstrong(int num,int sum){
		if(num==0){
			return sum;
		}
		int digit =num%10;
		return armstrong(num/10,sum+(int)Math.pow(digit,(int)Math.log10(num)+1));
	}
	static boolean isarmstong(int num){
		int sum=armstrong(num,0);
		return sum==num;
	}
	public static void main(String[] args){
		int num=163;
		if(isarmstrong(num)){
			System.out.println(num+"is armstrong number");
		}
		else{
			System.out.println(num+"is not armstrong number");
		}
	}
}

