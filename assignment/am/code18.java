//container with most water
import java.io.*;
class Water{
	public static int maxarea(int arr[]){
		int area=0;
		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				area=Math.max(area,Math.min((arr[i]*arr[j])*(j-i)));
			}
		}
		return area;
	}
	public static void main(String[] args){
		int arr[]={1,5,4,3};
		System.out.println(maxarea(arr));
	}
}


