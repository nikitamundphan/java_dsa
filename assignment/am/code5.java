//kth largest element
class Largest{
	static void swap(int arr[],int a,int b){
		int temp=arr[a];
		arr[a]=arr[b];
		temp=arr[b];
	}
	static int partition(int arr[],int l,int r){
		int x=arr[r];
		int i=1;
		for(int j=1;j<=r-1;j++){
			if(arr[j]<=x){
				swap(arr,i,j);
				i++;
			}
		}
		swap(arr,i,r);
		return i;
	}


	static int large(int arr[],int l,int r,int k,int n){
		int pos=partition(arr,l,r);
		if(pos-1==k-1){
			return -1;
		}
		else if(pos-1<k-1){
			large(arr,pos+1,r,k-pos+1-1,n);
		}
		else{
			large(arr,1,pos-1,k,n);
		}
	}
	public static void main(String[] args){
		int arr[]={};
		int k=3;
		int n=arr.length;

		large(arr,0,n-1,k,n);
		System.out.println(k+"largest element are:");
		for(int i=n-1;i>=n-k;i--){
			System.out.println(arr[i]);
		}
	}
}



