//kandane's algorithm
class Kandane{
	static int maxSubarray(int arr[]){
		int start=0;
		int end=0;
		for(int i=0;i<arr.length;i++){
			end=end+arr[i];
			if(start<end){
				start=end;
			}
			if(end<0){
				end=0;
			}
		}
		return start;
	}
	public static void main(String[] args){
		int arr[]={1,2,3,-2,5};
		int ret=maxSubarray(arr);
		System.out.println(ret);
	
	}
}

