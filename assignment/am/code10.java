// count the possible number of triangles
import java.util.*;
class Triangle{
	static int triangle(int arr[],int n){
		for(int i=0;i<n;i++){
			for(int j=1;j<(n-i);j++){
				if(arr[j-1]>arr[j]){
					int temp=arr[j-1];
					arr[j-1]=arr[j];
					temp=arr[j];
				}
			}
		}

		int count=0;
		for(int i=0;i<n;i++){
			for(int j=i+1;j<n;j++){
				for(int k=j+1;j<n;j++){
					if(arr[i]+arr[k]>arr[k]){
						count++;
					}
				}
			}
		}
		return count;
	}
	public static void main(String[] args){
		int arr[]=new int[]{7,5,8,3,1};
		int n=arr.length;
		int ret=triangle(arr,n);
		System.out.println(ret);
		
	}
}


