// nuts bolts problem
class NutDemo{
	public static void main(String[] args){

	char nuts={'@','#','$','%','^','&'};
	char bolts={'$','%','&','^','@','#'};
	matchpairs(nuts,bolts,0,5);
	System.out.println("matched nut and bolts are :");
	printarray(nuts);
	printarray(bolts);
}
static void printarray(char arr[]){
	for(char ch:arr){
		System.out.print(ch+" ");
	}
	System.out.println("\n");
}
static void matchpairs(char[] nuts,char[]bolts,int high,int low){
	if(low<high){
		int pivot=partition(nuts,low,high,bolts[high]);
		matchpairs(nuts,bolts,low,pivot-1);
		matchpairs(nuts,bolts,pivot+1,high);
	}
}
static int partition(char[]arr,int low,int high,char pivot){
	int i=low;
char temp1,temp2;
for(int j=low;j<high;j++){
	if(arr[j]<pivot){
		temp1=arr[i];
		arr[i]=arr[j];
		arr[j]=temp1;
		i++;
	}
	else if(arr[j]==pivot){
		temp1=arr[j];
		arr[j]=arr[high];
		arr[high]=temp1;
		j--;
	}
}
temp2=arr[i];
arr[i]=arr[high];
arr[high]=temp2;
return i;
}
}



