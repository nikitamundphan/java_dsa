//row with max 1s
class Row{
	static int maxrow(int arr[][],int r,int c){
		int max=0;
		int maxindex=0;
		for(int i=0;i<r;i++){
			int count=0;
			for(int j=0;j<c;j++){
				if(arr[i][j]==1){
					count++;
				}
			}
			if(count>max){
				max=count;
				maxindex=i;
			}
		}
		return maxindex;
	}
	public static void main(String[] args){
		int mat[][]={{0,1,1,1},{0,0,1,1},{1,1,1,1},{0,0,0,0}};
		int r=4;
		int c=4;
		int ret=maxrow(mat,r,c);
		System.out.println(ret);
	}
}


