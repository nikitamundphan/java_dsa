//zero sum subarrays
class SumArray{
	static int Subarray(int []arr,int n){
		for(int i=0;i<n;i++){
			int x=0;
			for(int j=i;j<n;j++){
				x+=arr[j];
				if(x==0){
					return (i,j);
				}
			}
		}
		return;
	}
	public static void main(String[] args){
		int arr[]=new int[]{0,0,5,5,0,0};
		int n=arr.length;
		Subarray(arr,n);
	}
}

