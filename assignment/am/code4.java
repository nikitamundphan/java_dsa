//kth smallest element
class Smallest{
	static int count(int arr[],int mid){
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]<=mid){
				count++;
			}
		}
		return count;
	}


	static int Kthelement(int arr[],int k,int n){
		int low=-1;
		int high=n;
		for(int i=0;i<n;i++){
			while(low<high){
				int mid=low+(high-low)/2;
				if(count(arr,mid)<k){
					low=mid+1;
				}

			       else{
				       high=mid;
			       }
			}
		}

			       return low;
	}

	public static void main(String[] args){
		int arr[]={7,10,4,3,5,20,15};
		int k=3;
		int n=arr.length;
		System.out.println(Kthelement(arr,k,n));

	}
}
