//print first 10 natural number in reverse order
class Demo{
	int fun(int num){
		if(num<=0){
			return 0;
		}
		System.out.println(num);
		return fun(--num);
	}
	public static void main(String[] args){
		Demo obj= new Demo();
		obj.fun(10);
	}
}


