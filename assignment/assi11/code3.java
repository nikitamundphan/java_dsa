// print the sum of n natural number
class Demo{
	int fun(int num){
		if(num!=0){
			return num+fun(num-1);
		}
		else{
			return num;
		}
	}
	public static void main(String[] args){
		Demo obj=new Demo();
		int sum=obj.fun(10);
		System.out.println(sum);
	}
}



