import java.io.*;
class Odd{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the size:");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		int mul=1;
		System.out.println("enter the elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(i%2!=0){
				mul=mul*arr[i];
			}
		}
		System.out.println("the product odd index elements ="+mul);

	}
}

