import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the size:");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		int evensum=0;
		int oddsum=0;
		System.out.println("enter the elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				evensum=evensum+arr[i];
			}
			else{
				oddsum=oddsum+arr[i];
			}
		}
		System.out.println("the even sum is="+evensum);
		System.out.println("the odd number sum is="+oddsum);
	}
}

