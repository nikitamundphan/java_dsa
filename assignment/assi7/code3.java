import java.io.*;
class Demo{
	static int Index(int arr[],int key){
		for(int i=0;i<arr.length;i++){
			if(arr[i]==key){
				return i;
			}
		}
		return -1;
	}


	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the size:");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("enter the key:");
		int key=Integer.parseInt(br.readLine());
		System.out.println("enter the elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int n=Index(arr,key);
		System.out.println(n);
	}
}


