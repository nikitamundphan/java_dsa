//queue reversal
import java.util.*;
class QueueDemo{
	int front;
	int rear;
	int queueArr[];
	int maxsize;
	QueueDemo(int size){
		this.queueArr=new int[size];
		this.front=-1;
		this.rear=-1;
		this.maxsize=size;
	}
	void enqueue(int data){
		if(rear==maxsize-1){
			System.out.println("queue is full");
		}
		else{
			rear++;
			queueArr[rear]=data;
		}
	}
	int dequeue(){
		if(front==-1){
			System.out.println("queue is empty");
			return -1;
		}
		else{
			int val=queueArr[front];
			front++;
			if(rear<front){
				rear=front=-1;
			}
			return val;
		}
	}
	void queuereversal(){
		if(rear==-1){
			System.out.println("queue is empty");
		}
		else{
			for(int i=rear;i>front;i--){
				System.out.print(queueArr[i]+" ");
			}
			System.out.println();
		}
	}
}
class Client{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("enter the array size");
		int size=sc.nextInt();
		QueueDemo q =new QueueDemo(size);
		char ch;
		do{
			System.out.println("1.enqueue");
			System.out.println("2.dequeue");
			System.out.println("3.queuereversal");
			System.out.println("enter your choice");
			int choice=sc.nextInt();
			switch(choice){
				case 1:
					{
						System.out.println("enter the data for enqueue");
						int data=sc.nextInt();
						q.enqueue(data);
					}
					break;

				case 2:
					{
						int ret=q.dequeue();
						if(ret!=-1){
							System.out.println("dequeued" +ret);
						}
					}
					break;

				case 3:
					q.queuereversal();
					break;

				default:
					System.out.println("wrong choice");
					break;

			}
			System.out.println("do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
	}
}

			

