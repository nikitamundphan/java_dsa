//queue using two stacks
import java.util.*;
class QueueDemo{
	Stack<Integer> s1=new Stack<Integer>();
	Stack<Integer> s2=new Stack<Integer>();
	void enqueue(int data){
		while(!s1.isEmpty()){
			s2.push(s1.pop());
		}
		s1.push(data);
		while(!s2.isEmpty()){
			s1.push(s2.pop());
		}
	}
	int dequeue(){
		if(s1.isEmpty()){
			return -1;
		}
		else{
			int x=s1.peek();
			s1.pop();
			return x;
		}
	}
}
class Client{
	public static void main(String[] args){
		QueueDemo qd=new QueueDemo();
		Scanner sc=new Scanner(System.in);
		char ch;
		do{
			System.out.println("1.enqueue");
			System.out.println("2.dequeue");
			System.out.println("enter your choice");
			int choice=sc.nextInt();
			switch(choice){
				case 1:
					{
						System.out.println("enter the data for enqueue");
						int data=sc.nextInt();
						qd.enqueue(data);
					}
					break;

				case 2:
					{
						int ret=qd.dequeue();
						if(ret!=-1){
							System.out.println("dequeued" +ret);
						}
					}
					break;
				default:
					System.out.println("wrong choice");
					break;
			}
			System.out.println("do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
	}
}



