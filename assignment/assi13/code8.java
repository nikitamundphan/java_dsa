//maintain sorted array order
class Demo{
	static int sortedarray(int arr[],int k){
		for(int i=0;i<arr.length;i++){
			if(arr[i]==k){
				return i;
			}
			else if(arr[i]>k){
				return i;
			}
		}
		return arr.length;
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,3,5,6};
		int k=2;
		System.out.println(sortedarray(arr,k));
	}
}


