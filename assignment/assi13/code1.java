//wap for binary search using for loop or binary search
class Demo{
	static int search(int arr[],int key){
		for(int i=0;i<arr.length;i++){
			if(arr[i]==key){
				return i;
			}
		}return -1;
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,2,3,4,5,6,7};
		int key=5;
		int ret=search(arr,key);
	}
}

