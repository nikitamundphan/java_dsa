//find the clpset element to a given target in a sorted array
class Demo{
	static int close(int arr[],int k){
		for(int i=0;i<arr.length;i++){
			if(arr[i]!=k){
				if(arr[i]>k){
					return arr[i];
				}
			}
		}
		return -1;
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,2,4,7,9};
		int k=6;
		int ret=close(arr,k);
		System.out.println(ret);
	}
}


