//find the starting and ending position of a given target value
class Index{
	static int findindex(int arr[],int t){

		if(arr==null){
			return -1;
		}
		int len=arr.length;
		int i=0;
		while(i<len){
			if(arr[i]==t){
				return i;
			}
			else{
				i=i+1;
			}
		}
		return -1;
	}
	public static void main(String[] args){
		int arr[]=new int[]{5,4,6,1,3,2,7,8,9};
		int t=5;
		System.out.println(findindex(arr,t));
	}
}

