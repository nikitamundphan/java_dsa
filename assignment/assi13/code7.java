//count the 1s in the given array
class Count{
        	static int count1s(int arr[],int n,int m){
			if(m>=n){
				int mid =n+(m-n)/2;
		
			if((mid==m) || arr[mid+1]==0) &&(arr[mid]==1){
				return mid+1;
			}
			if(arr[mid]==1){
				return count1s(arr,(mid+1),m);
			}
			return count1s(arr,n,(mid-1));
		}
		return 0;
		}

	
	public static void main(String[] args){
		int arr[]=new int[]{0,1,1,1,1,1};
		int n=arr.length;
		System.out.println(count1s(arr,0,n-1));
	}
}

