import java.io.*;
class LargeDemo{
	public static int Max(int arr[],int size){
		int temp;
		for(int i=0;i<size;i++){
			for(int j=i+1;j<size;j++){
				if(arr[i]>arr[j]){
					temp=arr[i];
					arr[i]=arr[j];
					temp=arr[j];
				}
			}
		}
		return arr[size-1];
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the size:");
		int size=Integer.parseInt(br.readLine());
		System.out.println("enter the element:");
		int arr[]=new int[size];
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int max=Max(arr,size);
		System.out.println("the largest number in array:"+max);
	}
}




