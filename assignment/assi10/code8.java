import java.io.*;
class CharDemo{
	static char largechar(String str){
		char max='A';
		for(int i=0;i<str.length();i++){
			if(str.charAt(i)>max){
				max=str.charAt(i);
			}
			return max;
		}
		return 0;
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the string");
		String str=br.readLine();
		char ch=largechar(str);
		System.out.println("the largest character in string is:"+ch);
	}
}


