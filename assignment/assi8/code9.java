//to find the second large number of array 
import java.io.*;
class Second{
	static int large(int arr[]){
		int temp;
		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				if(arr[i]>arr[j]){
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
		return arr[arr.length-2];
	}
		  
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("enetr the elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int n=large(arr);
		System.out.println(n);
		}
}


