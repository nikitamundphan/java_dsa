//to find prime number in array and returns its index
import java.io.*;
class Prime{
	static int pro(int arr[]){
		int count=0;
		for(int i=0;i<arr.length;i++){
		  for(int j=1;j<arr[i];j++){
			if(arr[i]%j==0){
				count++;
			}
			if(count == 2){
				return i;
			}
		}
		}
		return -1;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("enetr the elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int n=pro(arr);
		System.out.println("the prime number is found at index "+n);
	}
}


