//divide string into n equals part where the string length is multiple of n
class Demo{
	static void divide(String str,int n){
		if(str.length()%n!=0){
			System.out.println("is not divisible by n");
			return;
		}
		int parts=str.length()/n;
		int start=0;
		int t=parts;
		while(start<str.length()){
			String temp=new String(str);
			System.out.println(temp.substring(start,parts)+"\n");
			start=parts;
			parts+=t;
		}
	}
	public static void main(String[] args){
		String str="i am nikita";
		divide(str,4);
	}
}

