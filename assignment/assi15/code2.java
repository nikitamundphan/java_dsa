//replace a in string with l
class Demo{
	static void replace(char[] str,char ch1,char ch2){
		int j=0;
		int n=str.length();
		for(int i=0;i<n;i++){
			if(str[i]!=ch1){
				str[j++]=str[i];
			}
			else{
				str[j++]=ch2;
			}
		}
	}
	public static void main(String[] args){
		char str[]="nikita".toCharArray();
		char ch1='i';
		char ch2='u';
		replace(str,ch1,ch2);
		System.out.println(str);
	}
}

