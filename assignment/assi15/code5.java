//compare two string if the same return true else false
class Demo{
	static int stringcompare(String str,String str2){
		int l1=str.length();
		int l2=str2.length();
		int lmin=Math.min(l1,l2);
		for(int i=0;i<lmin;i++){
			int str_ch=(int)str.charAt(i);
			int str2_ch=(int)str2.charAt(i);
			if(str_ch!=str2_ch){
				return str_ch-str2_ch;
			}
		}
		if(l1!=l2){
			return l1-l2;
		}
		else{
			return 0;
		}
	}
	public static void main(String[] args){
		String str=new String("nikita");
		String str2=new String("NIKITA");
		System.out.println("comparing" +str +"and" +str2+ ":" +stringcompare(str,str2));
	}
}
