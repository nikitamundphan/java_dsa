//string is anagram
import java.util.*;
class Demo{
	static boolean anagram(char[] str1,char[] str2){
		int n1=str1.length;
		int n2=str2.length;
		if(n1!=n2){
			return false;
		}
		for(int i=0;i<n1;i++){
			if(str1[i]!=str2[i]){
				return false;
			}

		}
		return true;
	}
	public static void main(String[] args){
		char str1[]={'g','r','a','m'};
		char str2[]={'a','r','m'};
		if(anagram(str1,str2)){
			System.out.println("the string are anagram to each other");
		}
		else{
			System.out.println("the string are not anagram to each other");
		}
	}
}

