//print all permutation of String
class Demo{
	static void print(String str,String ans){
		if(str.length()==0){
			System.out.println(ans+" ");
		}
		for(int i=0;i<str.length();i++){
			char ch=str.charAt(i);
			String ros=str.substring(0,i)+str.substring(i+1);
			print(ros,ans+ch);
		}
	}

	public static void main(String[] args){
		String str = "abb";
		print(str,"");
	}
}

