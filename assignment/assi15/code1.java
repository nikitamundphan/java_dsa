//wap to count vowels in astring
class Demo{
	static int vowels(char ch){
		if(ch=='A' || ch=='E'||ch=='I' ||ch=='O' ||ch=='U'){
			return 1;
		}
		else {
			return 0;
		}
	}
		static int count(String str,int n){
			int n=str.length();
			if(n==1){
				return vowels(str.charAt(n-1));
			}
			return count(str,n-1)+vowels(str.charAt(n-1));
		}
	public static void main(String[] args){
		String str="Nikita";
		System.out.println(count(str));
	}
}

