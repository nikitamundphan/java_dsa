//convert all chracter to uppercase
class Demo{
	static void uppercase(StringBuffer str){
		int n=str.length();
		for(int i=0;i<n;i++){
			Character c=str.charAt(i);
			if(Character.isLowerCase(c)){
				str.replace(i,i+1,Character.toUpperCase(c)+ " ");
			}
			else{
				str.replace(i,i+1,Character.toLowerCase(c));
			}
		}
	}
	public static void main(String[] args){
		StringBuffer str=new StringBuffer("nikita");
		uppercase(str);
		System.out.println(str);
	}
}


