//implement stack using array
import java.util.*;
class StackArr{
	int top;
	int maxsize;
	int stackArr[];
	StackArr(int size){
		this.stackArr=new int[size];
		this.top=-1;
		this.maxsize=size;
	}
	void push(int data){
		if(top==maxsize){
			System.out.println("stack overflow");
		}
		else{
			top++;
			stackArr[top]=data;
		}
	}
	int pop(){
		if(top==-1){
			System.out.println("stack is empty");
			return -1;
		}
		else{
			int ret=stackArr[top];
			top--;
			return ret;
		}
	
	}
	void printstack(){
		for(int i=0;i<maxsize;i++){
			System.out.print(stackArr[top]+ ",");
		}
		System.out.println();
	}

		
}
class Client{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("enter the array size");
		int size=sc.nextInt();
		StackArr obj= new StackArr(size);
		char ch;
		do{
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.printstack");
			System.out.println("enter your choice");
			int choice=sc.nextInt();
			switch(choice){
				case 1:
					{
						System.out.println("enetr the data");
						int data = sc.nextInt();
						obj.push(data);
					}
					break;

				case 2:
					int ret= obj.pop();
					if(ret!=-1){
						System.out.println(ret+"popped");
					}
					break;

				case 3:
					obj.printstack();
					break;

				default:
					System.out.println("wrong choice");
					break;

			}
			System.out.println("do you want to continue");
			ch =sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
	}
}



			
