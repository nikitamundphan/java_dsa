//stack designer
import java.util.*;
class Designer{
	int top;
	int stackArr[];
	int maxsize;
	Designer(int size){
		this.stackArr=new int[size];
		this.top=-1;
		this.maxsize=size;
	}
	void push(int data){
		if(top==maxsize){
			System.out.println("stack is overflow");
		}
		else{
			top++;
			stackArr[top]=data;
		}
	}
	int pop(){
		if(top==-1){
			System.out.println("stack is empty");
			return -1;
		}
		else{
			int val=stackArr[top];
			top--;
			return val;
		}
	}
}
class Client{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the size for array");
		int size=sc.nextInt();
		Designer d= new Designer(size);
		char ch;
		do{
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("enter the choice");
			int choice = sc.nextInt();
			switch(choice){
				case 1:
					{
						System.out.println("enter the data");
						int data=sc.nextInt();
						d.push(data);
					}
					break;
				case 2:
					int val=d.pop();
					System.out.println(val);
					break;

				default:
					System.out.println("wrong choice");
					break;
			}
			System.out.println("do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
	}
}



