//reverse string using stack
import java.util.*;
class Reversestack{
	String reverse(String str){
		Stack<Character>s =new Stack<Character>();
		for(int i=0;i<str.length();i++){
			s.push(str.charAt(i));
		}
		char stackArr[]=new char[str.length()];
		int i=0;
		while(!s.empty()){
			stackArr[i]=s.pop();
			i++;
		}
		return new String(stackArr);
	}
}
class Client{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the string");
		String str=sc.next();
		Reversestack s =new Reversestack();
		String rev = s.reverse(str);
		System.out.println(rev);
	}
}
