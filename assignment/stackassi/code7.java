//special stack
import java.util.*;
class Special{
	int top;
	int stackArr[];
	int maxsize;
	Special(int size){
		this.stackArr= new int[size];
		this.top=-1;
		this.maxsize=size;
	}
	void push(int data){
		if(top==maxsize){
			System.out.println("stack overflow");
		}
		else{
			top++;
			stackArr[top]=data;
		}
	}
	void specialstack(){
		for(int i=0;i<maxsize;i++){
			for(int j=1;j<maxsize-1;j++){
				if(stackArr[i]<stackArr[j]){
					System.out.println(stackArr[i]);
			}
		}
	}
	}
}
class Client{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("enter the size");
		int size=sc.nextInt();
		Special s =new Special(size);
		char ch;
		do{
			System.out.println("1.push");
			System.out.println("2.specialstack");
			System.out.println("enter your choice");
			int choice=sc.nextInt();
			switch(choice){
				case 1:
					{
						System.out.println("enter the data");
						int data=sc.nextInt();
						s.push(data);
					}
					break;

				case 2:
					s.specialstack();
					break;
				default:
					System.out.println("wrong choice");
					break;
			}
			System.out.println("do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
	}
}



