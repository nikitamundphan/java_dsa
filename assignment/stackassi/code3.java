//implement two stacks in an array
import java.util.*;
class StackArr{
	int top1;
	int top2;
	int stackarr[];
	int maxsize;
	StackArr(int size){
		this.stackarr=new int[size];
		this.top1=-1;
		this.top2=maxsize;
		this.maxsize=size;
	}
	void push1(int data){
		if(top2-top1>1){
			top1++;
			stackarr[top1]=data;
		}
		else{
			System.out.println("stack overflow");
		}
	}
	void push2(int data){
		if(top2-top1>1){
			top2--;
			stackarr[top2]=data;
		}
		else{
			System.out.println("stack overflow");
		}
	}
	int pop1(){
		if(top1==-1){
			System.out.println("stack1 is empty");
			return -1;
		}
		else{
			int ret=stackarr[top1];
			top1--;
			return ret;
		}
	}
	int pop2(){
		if(top2==maxsize){
			System.out.println("stack2 is empty");
			return -1;
		}
		else{
			int val=stackarr[top2];
			top2++;
			return val;
		}
	}
}
class Client{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("enter size of array");
		int size=sc.nextInt();
		StackArr sa= new StackArr(size);
		char ch;
		do{
			System.out.println("1.push1");
			System.out.println("2.push2");
			System.out.println("3.pop1");
			System.out.println("4.pop2");
			System.out.println("enter your choice");
			int choice= sc.nextInt();
			switch(choice){
				case 1:
					{
						System.out.println("enter the data for stack1");
						int data=sc.nextInt();
						sa.push1(data);
					}
					break;

				case 2:
					{
						System.out.println("enter data for stack2");
						int data=sc.nextInt();
						sa.push2(data);
					}
					break;
				case 3:
					int ret=sa.pop1();
					if(ret!=-1){
						System.out.println(ret +"popped");
					}
					break;
				case 4:
					int ret2=sa.pop2();
					if(ret2!=-1){
						System.out.println(ret2 +"popped");
					}
					break;

				default:
					System.out.println("wrong choice");
					break;

			}
			System.out.println("do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch=='y'|| ch=='Y');
	}

}





