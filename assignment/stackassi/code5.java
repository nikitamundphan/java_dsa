//implement stack using linked list
class LinkedList{

	class Node{
		int data;
		Node next=null;
	}
	Node top;
	LinkedList(){
		this.top=null;
		
	}
	void push(int data){
		Node newnode=new Node();
		if(top==null){
			System.out.println("stack overflow");
		}
		newnode.data=data;
		newnode.next=top;
		top=newnode;
	}
	boolean isEmpty(){
		if(top==null){
			return true;
		}
		else{
			return false;
		}
	}
	int peek(){
		if(!isEmpty()){
			return top.data;
		}
		else{
			System.out.println("stack is empty");
			return -1;
		}
	}
	void pop(){
		if(top==null){
			System.out.println("stack is empty");
		}
		else{
			top=top.next;
		}
	}
	void display(){
		if(top==null){
			System.out.println("stack is empty");
		}
		else{
			Node temp=top;
			while(temp!=null){
				System.out.println(temp.data);
				temp=temp.next;
				if(temp!=null){
					System.out.println("->");
				}
			}
		}
	}
}
class Client{
	public static void main(String[] args){
		LinkedList obj = new LinkedList();
		obj.push(11);
		obj.push(22);
		obj.push(33);
		obj.push(44);
		obj.display();
		System.out.println("top element is:" +obj.peek());
		obj.pop();
		obj.pop();
		obj.display();
	}
}





