//sort a stack
import java.util.*;

class Sortstack{
	 Stack<Integer>sortstack(Stack<Integer>s){
		Stack<Integer>temp =new Stack<Integer>();
		while(!s.isEmpty()){
			int x= s.pop();
			while(!temp.isEmpty() && temp.peek()<x){
				s.push(temp.pop());
			}
			temp.push(x);
		}
		return temp;
	}
}
class Client{
	public static void main(String[] args){
		Stack<Integer> s= new Stack<Integer>();
		s.push(80);
		s.push(83);
		s.push(45);
		s.push(61);
		Sortstack ss =new Sortstack();
		Stack<Integer> stack = ss.sortstack(s);
		while(stack.empty()){
			System.out.println(stack.pop()+" ");
		}
	}
}



