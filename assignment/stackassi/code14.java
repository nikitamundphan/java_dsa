//insert element at the bottom of a stack
import java.util.*;
class Insert{
	void insert(Stack<Integer>s,int n){
		Stack<Integer> temp=new Stack<Integer>();
		while(!s.empty()){
			temp.push(s.peek());
			s.pop();
		}
		s.push(n);
		while(!temp.empty()){
			s.push(temp.peek());
			temp.pop();
		}
		while(!s.empty()){
			System.out.println(s.peek()+ " ");
			s.pop();
		}
	}
}
class Client{
	public static void main(String[] args){
		Stack<Integer>s =new Stack<Integer>();
		s.push(5);
		s.push(4);
		s.push(3);
		s.push(2);
		s.push(1);
		int n=7;
		Insert i =new Insert();
		i.insert(s,n);
		
	}
}

	
