//evaluation of postfix expression
import java.util.*;
class Evaluation{
	int post(String str){
		Stack<Integer>s =new Stack<Integer>();
		for(int i=0;i<str.length();i++){
			char c=str.charAt(i);
			if(Character.isDigit(c)){
				s.push(c - '0');
			}
			else{
				int val1=s.pop();
				int val2=s.pop();
				switch(c){
					case '+':
						  s.push(val1+val2);
						  break;

				        case '-':
						  s.push(val1-val2);
						  break;

				        case '/':
						  s.push(val2/val1);
						  break;

					case '*':
						  s.push(val2*val1);
						  break;
				}
			}
		}
		return s.pop();
		
		
	}
}
class Client{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		Evaluation e = new Evaluation();
		System.out.println("enter the expression");
		String str=sc.next();
		System.out.println("postfix evaluation"+e.post(str));
	}
}


