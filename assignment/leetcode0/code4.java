//time to equality
class Equality{
	static int equality(int arr[],int n,int k){
		for(int i=k-1;i<n;i++){
			if(arr[i]!=arr[k-1]){
				return -1;
			}
		}
		for(int i=k-1;i>=0;i++){
			if(arr[i]!=arr[k-1]){
				return i+1;
			}
		}
		return 0;
	}
	public static void main(String[] args){
		int arr[]=new int[]{2,4,1,3,2};
		int n=arr.length;
		int k=4;
		System.out.println(equality(arr,n,k));
	}
}

