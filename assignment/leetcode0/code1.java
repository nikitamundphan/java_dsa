//max min of an array
class Demo{
	static void  sum(int arr[],int n){
		int max=Integer.MIN_VALUE;
		int min=Integer.MAX_VALUE;
		for(int i=0;i<n;i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		for(int j=0;j<n;j++){
			if(arr[j]<min){
				min=arr[j];
			}
		}
		System.out.println(max+min);
	}
	public static void main(String[] args){
		int arr[]=new int[]{-2,1,-4,5,3};
		int n=arr.length;
		sum(arr,n);
	}
}

