//range sum query
class Range{
	static void sum(int arr[],int n,int prex[]){
		prex[0]=arr[0];
		for(int i=1;i<n;i++){
			prex[i]=arr[i]+prex[i-1];
		}
	}
	static int rangesum(int i,int j,int prex[]){
		if(i==0){
			return prex[j];
		}
		return prex[j]-prex[i-1];
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,2,3,4,5};
		int n=arr.length;
		int prex[]=new int[n];
		sum(arr,n,prex);
		System.out.println(rangesum(1,3,prex));
		System.out.println(rangesum(2,4,prex));
	}
}

		

