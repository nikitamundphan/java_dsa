//in-place prefix sum
class Demo{
	static void prefsum(int arr[],int n){
		int sum[]=new int[n];
		sum[0]=arr[0];
		for(int i=1;i<n;i++){
			sum[i]=arr[i]+sum[i-1];
		}
		for(int i=0;i<n;i++){
			System.out.println(sum[i]);
		}
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,2,3,4,5};
		int n=arr.length;
		prefsum(arr,n);
	}
}


