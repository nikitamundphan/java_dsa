//reverse the subaraay
class Demo{
	static void reverse(int arr[],int n,int k){
		for(int i=0;i<n;i++){
			int left=i;
			int right=Math.min(i+k-1,n-1);
			int temp;
			while(left<right){
				temp=arr[left];
				arr[left]=arr[right];
				arr[right]=temp;
				left++;
				right++;
			}
		
		
		}
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,2,3,4,5,6,7,8,9};
		int n = arr.length;
		int k=3;
		reverse(arr,n,k);
		for(int i=0;i<n;i++){
			System.out.println(arr[i]);
		}
	}
}




