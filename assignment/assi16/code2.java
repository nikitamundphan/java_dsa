//equal left and right subarray sum
class Demo{
	static int equalsum(int arr[],int n){
		int prefsum[]=new int[n];
		for(int i=0;i<n;i++){
			prefsum[i]=prefsum[i-1]+arr[i];
		}
		int suffsum[]=new int[n];
		suffsum[n-1]=arr[n-1];
		for(int i=n-2;i>=0;i--){
			suffsum[i]=suffsum[i+1]+arr[i];
		}
		for(int i=0;i<n;i++){
			if(prefsum[i]==suffsum[i]){
				return arr[i];
			}
		}
		return -1;
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,4,2,5};
		int n=arr.length;
		System.out.println(equalsum(arr,n));
	}
}


