//find max sum strictly increasing subarray
class Demo{
	static int maxsum(int arr[],int n){
		int max=arr[0];
		int curr_sum=arr[0];
		for(int i=0;i<n;i++){
			if(arr[i-1]<arr[i]){
				curr_sum=curr_sum+arr[i];
				max=Math.max(max,curr_sum);
			}
			else{
				max=Math.max(max,curr_sum);
				curr_sum=arr[i];
			}
		}
		return Math.max(max,curr_sum);
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,2,2,4};
		int n=arr.length;
		System.out.println(maxsum(arr,n));
	}
}


