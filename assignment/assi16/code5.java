// largest subaaray with sum 0;
class Demo{
	static int maxlen(int arr[],int n){
		int max=0;
		for(int i=0;i<n;i++){
			int curr_sum=0;
			for(int j=i;j<n;j++){
				if(curr_sum==0){
					max=Math.max(max,j-i+1);
				}
			}
		}
		return max;
	}
	public static void main(String[] args){
		int arr[]=new int[]{15,-2,2,-8,1,7,10,23};
		int n=arr.length;
		System.out.println(maxlen(arr,n));
	}
}


