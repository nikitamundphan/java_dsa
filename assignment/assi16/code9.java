//max of all subarray of size k
class Demo{
	static int findmax(int arr[],int n,int k){
		int max=Integer.MAX_VALUE;
		for(int i=1;i<n;i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		return max;
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,2,31,4,5,2,6};
		int n=arr.length;
		int k=3;
		System.out.println(findmax(arr,n,k));
	}
}


