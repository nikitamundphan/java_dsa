//subarray with the given sum
class Demo{
	static void subarray(int arr[],int sum){
		for(int i=0;i<arr.length;i++){
			int cursum=arr[i];
			if(cursum==sum){
				System.out.println("sum found at index"+i);
			}
			else{
				for(int j=i+1;j<arr.length;j++){
					cursum+=arr[j];
					System.out.println(j);
				}
			}
		}
	}
	public static void main(String[] args){
		int arr[]=new int[]{15,2,4,8,9,5,10,23};
		int sum=23;
		subarray(arr,sum);
	}
}


