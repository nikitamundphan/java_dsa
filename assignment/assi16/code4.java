//subaaray with 0 sum
class Demo{
	static boolean subarray(int arr[],int n){
		for(int i=0;i<n;i++){
			int sum=arr[i];
			if(sum==0){
				return true;
			}
			for(int j=i+1;j<n;j++){
				sum+=arr[j];
				if(sum==0){
					return true;
				}
			}
		}return false;
	}
	public static void main(String[] args){
		int arr[]=new int[]{-3,2,3,1,6};
		int n=arr.length;
		if(subarray(arr,n)){
			System.out.println("found the subarray with sum0");
		}
		else{
			System.out.println("not found the subaray with sum0");
		}
	}
}

