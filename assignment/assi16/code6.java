//smallest subarray with the sum greater than x
class Demo{
	static int small(int arr[],int n,int x){
		int min=n+1;
		for(int start=0;start<n;start++){
			int curr_sum=arr[start];
			if(curr_sum>x){
				return 1;
			}
			for(int end=start+1;end<n;end++){
				curr_sum+=arr[end];
				if(curr_sum>x && (end-start+1)<min){
					min=(end-start+1);
				}
			}
		}
		return min;
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,4,45,6,10,19};
		int n=arr.length;
		int x=51;
		int res=small(arr,n,x);
		if(res==n+1){
			System.out.println("not possible");
		}
		else{
			System.out.println(res);
		}
	}
}


