// maximum product subarray
class Demo{
	static int maxsubarray(int arr[]){
		int result=arr[0];
		int n= arr.length;
		for(int i=0;i<arr.length;i++){
			int mul=arr[i];
			for(int j=i+1;j<n;j++){
				result=Math.min(result,mul);
				mul*=arr[j];

			}
			result=Math.min(result,mul);
		}
		
		return result;
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,-2,-3,0,7,-8,-2};
		System.out.println(maxsubarray(arr));
	}
}

