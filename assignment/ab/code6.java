// elements in range
class RangeDemo{
	public static void main(String[] args){
		int arr[]=new int[]{1,2,3,4,5,6};
		int num=5;
		int start=2;
		int end=5;
		for(int i=start;i<end;i++){
			if(num==arr[i]){
				System.out.println("The num is between the range");
			}
			else{
				System.out.println("The num is not between the range");
			}
		}
	}
}

