//product of max of  first array and minimum of second array
class Demo{
	static void Product(int arr1[],int arr2[]){
		int max=arr1[0];
		for(int i=0;i<arr1.length;i++){
			if(max<arr1[i]){
				max=arr1[i];
			}
		}
		int min=arr2[0];
		for(int i=0;i<arr2.length;i++){
			if(min>arr2[i]){
				min=arr2[i];
			}
		
		}
		System.out.println(max*min);

	}
	public static void main(String[] args){
		int arr1[]={1,2,3,4,7};
		int arr2[]={7,8,9,10};
		Product(arr1,arr2);
	}
}

