//smallest and second smallest in array
import java.util.*;
import java.io.*;

class Small{
	static void small(int arr[]){
		Arrays.sort(arr);
		System.out.println("smallest element is :"+arr[0]);
		System.out.println("second smallest element :"+arr[1]);
	}
	public static void main(String[] args){
		int arr[]=new int[]{2,4,3,5,6};
		small(arr);
	}
}
		
