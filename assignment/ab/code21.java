//first element to occur k times
class Occurance{
	static int FirstElement(int arr[],int n){
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr.length;j++){
				if(arr[i]==arr[j]){
					count++;
				}
			}
			if(count==n){
				return arr[i];
			}
		}
		return -1;
	}
	public static void main(String[] args){
		int arr[]={1,7,4,3,4,8,7};
		int n=2;
		System.out.println(FirstElement(arr,n));
	}
}

