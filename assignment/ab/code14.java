class Demo{
	static int MaxRepeat(int arr[],int k){

		int count=0;
		for(int i=0;i<arr.length;i++){
			arr[(arr[i]%k)]+=k;
		}

			int max=arr[0];
			for(int i=1;i<arr.length;i++){
				if(arr[i]>max){
					max=arr[i];
					count=i;
				}
			}
			return count;
		}
	
	public static void main(String[] args){
		int arr[]=new int[]{2,2,1,0,0,1};
		int k=3;
		System.out.println("Maximum reapeating element is:"+MaxRepeat(arr,k));
	}
}



