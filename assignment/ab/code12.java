// first and last occurance of element
class Demo{

         static void OccuranceDemo(int arr[],int x){
	int first=-1;
	int last=-1;
	for(int i=0;i<arr.length;i++){
		if(x!=arr[i]){
			continue;
		}
		if(first==-1){
			first=i;
		}
		last=i;
	}
	if(first!=-1){
		System.out.println("first occurance is:"+first);
		System.out.println("last occurance is:"+last);
	}
	else{
		System.out.println("not found");

	
	}
	}

	public static void main(String[] args){
		int arr[]=new int[]{1,3,3,4};
		int x=3;
		OccuranceDemo(arr,x);
	}

}


