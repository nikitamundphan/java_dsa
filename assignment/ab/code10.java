class MaxOddSum{
	 int max(int arr[]){

		int sum=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]>0){
				sum=sum+arr[i];
			}
			else{
				return -1;
			}
			
		        if(sum%2!=0){
			     return sum;
		}

	}
	return 0;

	 }
}

class Demo{
	public static void main(String[] args){
		int arr[]=new int[]{2,5,-4,3,-1};
		MaxOddSum obj=new MaxOddSum();
		int n=obj.max(arr);
		System.out.println(n);

	}
}


