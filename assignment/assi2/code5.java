// 26 Z 25 Y
// 24 X 23 W
// 22 V 21 U
// 20 T 19 S
class Alpha{
	public static void main(String[] args){
		int row=4;
		int col=4;
		int num=26;
		char ch='Z';
		for(int i=1;i<=row;i++){
			for(int j=1;j<=col;j++){
				if(j%2==0){
					System.out.print(ch+" ");
					ch--;
				}
				else{
					System.out.print(num+" ");
					num--;
				}
			}
			System.out.println();
		}

	}
}

