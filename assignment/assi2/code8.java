//A b C d
//E f G h
//I j K l
class App{
	public static void main(String[] args){
		int row=4;
		int col=4;
		char ch1='A';
		char ch2='b';
		for(int i=1;i<=row;i++){
			for(int j=1;j<=col;j++){
				if(j%2==0){
					System.out.print(ch2+" ");
					ch2+=2;
				}
				else{
					System.out.print(ch1+" ");
					ch1+=2;
				}
			}
			System.out.println();
		}
	}
}




