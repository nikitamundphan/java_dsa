// 1 2 9
// 4 5 36
// 49 8 81
class Sqr{
	public static void main(String[] args){
		int row=3;
		int col=3;
		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=col;j++){
				if(j%2==0){
					System.out.print(num+" ");

				}
				else{
					System.out.print(num*num+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
