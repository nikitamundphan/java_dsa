//magical number
class Demo{
	static int magic(int arr[],int n){
		int i=0;
		int count=0;
		int j=0;
		int parent[]=new int[n+1];
		int []vis=new int[n+1];
		for(int i=0;i<n+1;i++){
			parent[i]=-1;
			vis[i]=0;
		}
		for(int i=0;i<n+1;i++){
			j=i;
			if(parent[j]==-1){
				while(parent[j]==-1){
					parent[j]=i;
					j=(j+arr[j]+1)%n;
				}
				if(parent[j]==i){
					while(vis[j]==0){
						vis[j]=1;
						count++;
						j=(j+arr[j]+1)%n;
					}
				}
			}
		}
		return count;
	}
	public static void main(String[] args){
		int arr[]=new int[]{0,0,0,2,0,0};
		int n=arr.length;
		int ret=magic(arr,n);
		System.out.println(ret);
	}
}


