//sorted insert position
class Demo{
	static int index(int arr[],int k){
		for(int i=0;i<arr.length;i++){
			if(arr[i]==k){
				return i;
			}
			else if(arr[i]>k){
				return i;
			}
		}
		return arr.length;
	}
	public static void main(String[] args){
		int arr[]={1,3,5,7};
		int k=6;
		System.out.println(index(arr,k));
	}
}


