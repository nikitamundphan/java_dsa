//a sorted array k times given an x element find its index of it in range array.
class Demo{
	static int findelement(int arr[],int[][]range,int rotations,int index){
		int n1=1;
		int i=0;
		int lrange=0;
		int rrange=0;
		int k=0;
		while(n1<=rotations){
			lrange=range[i][0];
			rrange=range[i][1];
			k=arr[rrange];
			for(int j=rrange;j>=lrange+1;j--){
				arr[j]=arr[j-1];
			}
			arr[lrange]=k;
			n1++;
			i++;
		}
		return arr[index];
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,2,3,4,5};
		int rotations=0;
		int [][]range={{0,2},{0,3}};
		int index=1;
		System.out.println(findelement(arr,range,rotations,index));
	}
}


