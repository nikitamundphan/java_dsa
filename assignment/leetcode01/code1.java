// reverse the integer
class Demo{
	static int reverse(int num){
		long x=num;
		long rev=0;
		long rem=0;
		while(x!=0){
			rem=x%10;
			rev=rev*10+rem;
			x=x/10;
		}
		if(rev > Integer.MAX_VALUE || rev<Integer.MIN_VALUE){
			return 0;
		}
		return(int) rev;
	}
	public static void main(String[] args){
		int num=323;
		int n=reverse(num);
		System.out.println(n);
	}
}



