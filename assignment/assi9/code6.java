import java.io.*;
class Count{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("how many number you want to enter");
		int input=Integer.parseInt(br.readLine());
		int count=0;
		for(int i=1;i<=input;i++){
			int num=Integer.parseInt(br.readLine());
			int temp=num;
			while(temp!=0){
				count++;
				temp=temp/10;
			}
			System.out.println("number "+num+ "has count" +count+ "of digits");

		}
	}
}

